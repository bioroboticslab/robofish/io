# SPDX-License-Identifier: LGPL-3.0-or-later

from subprocess import run, PIPE

from setuptools import setup, find_packages


entry_points = {
    "console_scripts": [
        "robofish-io-validate=robofish.io.app:validate",
        "robofish-io-print=robofish.io.app:print_file",
        "robofish-io-render=robofish.io.app:render",
        # "robofish-io-clear-calculated-data=robofish.io.app:clear_calculated_data",
        "robofish-io-update-calculated-data=robofish.io.app:update_calculated_data",
        # TODO: This should be called robofish-evaluate which is not possible because of the package name (guess) ask moritz
        "robofish-io-evaluate=robofish.evaluate.app:evaluate",
        "robofish-io-update-individual-ids = robofish.io.app:update_individual_ids",
        "robofish-io-update-world-shape = robofish.io.app:update_world_shape",
        "robofish-io-overwrite_user_configs = robofish.io.app:overwrite_user_configs",
        "robofish-io-fix-switches = robofish.io.fix_switches:app_fix_switches",
    ]
}


def source_version():
    version_parts = (
        run(
            ["git", "describe", "--tags", "--dirty"],
            check=True,
            stdout=PIPE,
            encoding="utf-8",
        )
        .stdout.strip()
        .split("-")
    )

    version = version_parts[0]
    if len(version_parts) == 3:
        version += ".post0"
        version += f".dev{version_parts[1]}+{version_parts[2]}"

    return version


setup(
    name="robofish-io",
    version=source_version(),
    author="",
    author_email="",
    install_requires=[
        "h5py>=2.10.0",
        "numpy",
        "seaborn",
        "pandas",
        "deprecation",
        "tqdm",
        "pre-commit",
        "scipy",
    ],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering",
        "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    python_requires=">=3.6",
    packages=[f"robofish.{p}" for p in find_packages("src/robofish")],
    package_dir={"": "src"},
    zip_safe=True,
    entry_points=entry_points,
)

# Create the pre-commit hook with the system specific python executable
run(["pre-commit", "install"])
