## Documentation from docstrings

pdoc --html robofish.io robofish.evaluate --html-dir docs --force

## Code coverage:
pytest --cov=src --cov-report=html

## Flake
flake8 --ignore E203 --max-line-length 88
