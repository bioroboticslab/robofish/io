
<div align="center">
<a href="https://git.imp.fu-berlin.de/bioroboticslab/robofish/io/">
  <img alt="RobofishIO" title="RobofishIO" src="img/logo.svg" width="200">
</a>
</div>


[![pipeline status](https://git.imp.fu-berlin.de/bioroboticslab/robofish/io/badges/master/pipeline.svg)](https://git.imp.fu-berlin.de/bioroboticslab/robofish/io/commits/master)
# Robofish IO

This repository implements an easy to use interface, to create, save, load, and work with [specification-compliant](https://git.imp.fu-berlin.de/bioroboticslab/robofish/track_format) hdf5 files, containing 2D swarm data. This repository should be used by the different swarm projects to generate comparable standardized files.

## <a href="http://agerken.de/io/index.html">Documentation</a>

## Installation

Add our [Artifacts repository](https://git.imp.fu-berlin.de/bioroboticslab/robofish/artifacts) to your pip config and install the packagage.

```bash
python3 -m pip config set global.extra-index-url https://git.imp.fu-berlin.de/api/v4/projects/6392/packages/pypi/simple
python3 -m pip install robofish-io
```

## Usage
We show a simple example below. More examples can be found in ```examples/```

```python
import robofish.io
import numpy as np

# Create a new robofish io file
# Mode "w" means that the file should be opened with write access.
f = robofish.io.File(path, "w", world_size_cm=[100, 100], frequency_hz=25.0)
f.attrs["experiment_setup"] = "This is a simple example with made up data."

# Create a new robot entity with 10 timesteps.
# Positions and orientations are passed separately in this example.
# Since the orientations have two columns, unit vectors are assumed
# (orientation_x, orientation_y)
f.create_entity(
    category="robot",
    name="robot",
    positions=np.zeros((10, 2)),
    orientations=np.ones((10, 2)) * [0, 1],
)

# Create a new fish entity with 10 timesteps.
# In this case, we pass positions and orientations together (x, y, rad).
# Since it is a 3 column array, orientations in radians are assumed.
poses = np.zeros((10, 3))
poses[:, 0] = np.arange(-5, 5)
poses[:, 1] = np.arange(-5, 5)
poses[:, 2] = np.arange(0, 2 * np.pi, step=2 * np.pi / 10)
fish = f.create_entity("fish", poses=poses)
fish.attrs["species"] = "My rotating spaghetti fish"
fish.attrs["fish_standard_length_cm"] = 10

# Some possibilities to access the data
print(f"The file:\n{f}")
print(
    f"Poses Shape:\t{f.entity_poses_rad.shape}.\t"
    + "Representing(entities, timesteps, pose dimensions (x, y, ori)"
)
print(
    f"The actions of one Fish, (timesteps, (speed, turn)):\n{fish.actions_speeds_turns}"
)
print(f"Fish positions with orientations:\n{fish.poses_rad}")

```

### Evaluation
Evaluate your tracks with ```robofish-io-evaluate $mode $file(s)```
Current modes are:
- speed
- turn
- tank_positions
- trajectories
- follow_iid