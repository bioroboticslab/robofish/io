import robofish.io
import numpy as np


def create_example_file(path):
    # Create a new io file object with a 100x100cm world.
    # Mode "w" means that the file should be opened with write access.
    f = robofish.io.File(
        path, "w", world_size_cm=[100, 100], frequency_hz=25.0, world_shape="rectangle"
    )

    # create a robofish with 1000 timesteps. If we would not give a name, the name would be generated to be robot_1.
    robofish_timesteps = 1000
    robofish_poses = np.tile([50, 50, 1, 0], (robofish_timesteps, 1))
    robot = f.create_entity("robot", name="robot", poses=robofish_poses)

    # create multiple fishes with timestamps. Since we don't specify names, but only the type "fish" the fishes will be named ["fish_1", "fish_2", "fish_3"]
    agents = 3
    timesteps = 1000
    # timestamps = np.linspace(0, timesteps + 1, timesteps)
    agent_poses = np.random.random((agents, timesteps, 3))

    fishes = f.create_multiple_entities("fish", agent_poses)

    # This would throw an exception if the file was invalid
    f.validate()

    # Closing and opening files (just for demonstration). When opening with r+, we can read and write afterwards.
    f.close()
    f = robofish.io.File(path, "r+")

    print("\nEntity Names")
    print(f.entity_names)

    # Get an array with all poses. As the length of poses varies per agent, it is filled up with nans.
    print("\nAll poses")
    print(f.entity_poses)

    # Select all entities with the category fish
    print("\nFish poses")
    print(f.select_entity_poses(lambda e: e.category == "fish"))

    print("\nFinal file")
    print(f)


if __name__ == "__main__":
    create_example_file("example.hdf5")
