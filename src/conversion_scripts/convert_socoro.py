from pathlib import Path
import argparse

import numpy as np
import robofish.io
from tqdm import tqdm

from robofish.socoro.preprocessing.stats import load_trial


def convert(input_path: Path, output_path: Path):
    """
    Convert from csv format to RoboFish track format.

    robofish.socoro is used for the load_trial function.
    This is needed because the csv files do not have headers.
    Unused columns: "frame_number", "datetime_local".
    These columns could be used if needed, e.g. convert datetime_local to
    calendar_time_points or include frame_number if files will be split into segments.

    TODO: filter warning about unbounded radians
    """
    trial_df = load_trial(input_path)
    robot_poses = np.stack(
        [
            np.clip(trial_df.robot_position_x_cm, 0, 100) - 50,
            np.clip(trial_df.robot_position_y_cm, 0, 100) - 50,
            -trial_df.robot_orientation_rad,
        ]
    ).T
    guppy_poses = np.stack(
        [
            np.clip(trial_df.fish_position_x_cm, 0, 100) - 50,
            np.clip(trial_df.fish_position_y_cm, 0, 100) - 50,
            -trial_df.fish_orientation_rad,
        ]
    ).T

    f = robofish.io.File(world_size_cm=[100, 100], frequency_hz=25.0)

    f.create_entity(category="robot", poses=robot_poses, name="robot")
    f.create_entity(category="organism", poses=guppy_poses, name="guppy")

    for robot_dataset in [
        trial_df.robot_mode.astype(bytes),
        trial_df.avoidance_score,
        trial_df.follow_value,
        trial_df.carefulness_variable,
    ]:
        f["entities"]["robot"].create_dataset(robot_dataset.name, data=robot_dataset)
    f["samplings"]["25 hz"].create_dataset(
        "monotonic_time_points_us",
        data=(trial_df.monotonic_time_ms - trial_df.monotonic_time_ms[0]) * 1000,
    )
    f.save_as(output_path, no_warning=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Convert csv file from the socoro experiments to RoboFish track format."
    )
    parser.add_argument(
        "input", type=str, help="Single folder of files to be converted"
    )
    parser.add_argument("output", type=str, help="Output path")
    args = parser.parse_args()

    output_path = Path(args.output)
    output_path.mkdir(exist_ok=True)
    for path in tqdm(list(Path(args.input).glob("*.csv"))):
        convert(input_path=path, output_path=output_path / f"{path.stem}.hdf5")
