# -*- coding: utf-8 -*-

"""
This script converts csv files created by Marc to IO files.

The script is not maintained, since Marc changed his code to generate io files.
"""

# Jan 2021 Marc Groeling, Berlin, Germany
# Released under GNU 3.0 License
# email andi.gerken@gmail.com

# flake8: noqa

import pandas as pd
import numpy as np
import robofish.io


def convertTrajectory(path, save_path, categories):
    ar = pd.read_csv(path, sep=";").to_numpy()
    new = robofish.io.File(world_size_cm=[100, 100], frequency_hz=25)

    # convert x,y from m to cm
    ar[:, [0, 1, 3, 4]] = ar[:, [0, 1, 3, 4]] * 100
    big = np.argwhere(ar[:, [0, 1, 3, 4]] > 50)[:, 0]
    small = np.argwhere(ar[:, [0, 1, 3, 4]] < -50)[:, 0]
    for b in big:
        ar[b] = ar[b - 1]
    for s in small:
        ar[s] = ar[s - 1]

    new.create_entity(
        category=categories[0], positions=ar[:, [0, 1]], orientations=ar[:, [2]]
    )
    new.create_entity(
        category=categories[1], positions=ar[:, [3, 4]], orientations=ar[:, [5]]
    )

    new.save_as(save_path)
