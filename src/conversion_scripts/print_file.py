import robofish.io
import pandas as pd
import numpy as np


def print_file(args):

    # Open the robofish.io.File
    with robofish.io.File(args.filename, "r") as f:
        timesteps = (
            args.max_timesteps + args.skip_timesteps
            if args.max_timesteps is not None
            else f.entity_positions.shape[1]
        )

        n_fish = len(f.entities)
        df = pd.DataFrame(
            columns=np.concatenate([[f"x{i}", f"y{i}", f"o{i}"] for i in range(n_fish)])
        )

        for i in range(n_fish):
            df[f"x{i}"] = f.entity_positions[i][args.skip_timesteps : timesteps, 0]
            df[f"y{i}"] = f.entity_positions[i][args.skip_timesteps : timesteps, 1]
            df[f"o{i}"] = f.entity_orientations_rad[i][
                args.skip_timesteps : timesteps, 0
            ]

        print(df)

    df.to_csv("cutout.csv", index=False)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Print a file")
    parser.add_argument("filename", help="File to print")
    parser.add_argument("--skip_timesteps", type=int, default=0, help="Skip timesteps")
    parser.add_argument("--max_timesteps", type=int, default=None, help="Max timesteps")
    args = parser.parse_args()
    print_file(args)
