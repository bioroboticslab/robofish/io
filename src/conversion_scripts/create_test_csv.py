import pandas as pd
import numpy as np

# python convert_from_csv.py test.csv --sep , --header 1 --columns_per_entity 3 --xcol 0 --ycol 1 --oricol 2 --disable_fix_switches

df = pd.DataFrame(columns=["header", "x1", "y1", "o1", "x2", "y2", "o2"])

df["header"] = 0

df["x1"] = np.linspace(20, 80, 60)
df["x2"] = np.linspace(20, 80, 60)
df["y1"] = 40
df["y2"] = 60

# Switch y1 and y2 after 50 samples
df.loc[20:30, "y1"] = 60
df.loc[20:30, "y2"] = 40

# Set x1 and y1 to np.nan for 5 samples
df.loc[40:45, "x1"] = np.nan
df.loc[40:45, "y1"] = np.nan
df.loc[46:50, "y1"] = 60
df.loc[46:50, "y2"] = 40
df.loc[51:55, "x2"] = np.nan
df.loc[51:55, "y2"] = np.nan


df["o1"] = 180
df["o2"] = 180


print(df)
df.to_csv("test.csv", index=False)
