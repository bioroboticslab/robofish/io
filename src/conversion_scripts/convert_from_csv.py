# -*- coding: utf-8 -*-

"""
This script converts csv files from various sources to io files.

The script is not finished and still in progress.
"""

# Feb 2021 Andreas Gerken, Berlin, Germany
# Released under GNU 3.0 License
# email andi.gerken@gmail.com

# flake8: noqa

import pandas as pd
import numpy as np
import argparse
import itertools
from pathlib import Path
import robofish.io
import matplotlib.pyplot as plt

try:
    from tqdm import tqdm
except ImportError as e:
    print("tqdm not installed, not showing progress bar.")

    def tqdm(x):
        return x


def get_distances(poses, last_poses=None, diagonal=False):
    def get_single_distance(pose_1, pose_2):
        assert pose_1.shape == pose_2.shape == (3,)
        return np.linalg.norm(pose_1[:2] - pose_2[:2])

    if poses.ndim == 2:
        n_fish, three = poses.shape
        distances = np.zeros((n_fish, n_fish))
        for i in range(n_fish):
            for j in range(n_fish):
                if i == j or not diagonal:
                    if last_poses is None:
                        distances[i, j] = get_single_distance(poses[i], poses[j])
                    else:
                        distances[i, j] = get_single_distance(poses[i], last_poses[j])
    else:
        n_frames, n_fish, three = poses.shape
        distances = np.zeros((n_frames, n_fish, n_fish))

        for t in range(n_frames - 1):
            for i in range(n_fish):
                for j in range(n_fish):
                    if i == j or not diagonal:
                        assert last_poses is None
                        distances[t, i, j] = get_single_distance(
                            poses[t + 1, i], poses[t, j]
                        )
    if diagonal:
        return np.diagonal(distances, axis1=-1, axis2=-2)
    else:
        return distances


def handle_switches(poses, supress=None):
    """Find and handle switches in the data.
    Switches are defined as a fish that is not at the same position as it was in the previous frame.
    If a switch is found, the fish is moved to the position of the fish that was at the same position in the previous frame.
    If multiple switches are found, the fish that is closest to the position of the fish that was at the same position in the previous frame is moved.

    Args:
        poses (np.ndarray): Array of shape (n_frames, n_fish, 3) containing the poses of the fish.
        supress (list): List of frames to ignore.
    Returns:
        np.ndarray: Array of shape (n_frames, n_fish, 3) containing the poses of the fish with switches handled.
    """

    n_timesteps, n_fish, three = poses.shape

    all_switches = []

    last_poses = np.copy(poses[0])

    all_connection_permutations = list(itertools.permutations(np.arange(n_fish)))

    for t in range(1, poses.shape[0]):
        if np.all(np.isclose(np.abs(np.diff(poses[t - 1], axis=0)), 0)):
            print(
                f"Warning: All fish at same position in frame {t - 1} setting them to NaN"
            )
            poses[t - 1] = np.nan

        if supress is None or t not in supress:
            distances = get_distances(poses[t], last_poses)

            switches = {}
            for i in range(n_fish):
                if np.argmin(distances[i]) != i:
                    switches[i] = np.argmin(distances[i])

            if len(switches) > 0 or np.any(
                np.isnan(poses[t]) != np.isnan(poses[t - 1])
            ):
                switch_distances = np.array(
                    [
                        get_distances(poses[t, con_perm], last_poses, diagonal=True)
                        for con_perm in all_connection_permutations
                    ]
                )

                switch_distances_sum = np.nansum(switch_distances, axis=1)

                # if t > 10 and t < 18:
                #    print("Poses\n", poses[t], "\nLast Poses\n", last_poses)
                #    print(t, "\n", switch_distances[:2], "\n", switch_distances_sum[:2])

                connections = np.array(
                    all_connection_permutations[np.argmin(switch_distances_sum)]
                ).astype(int)

                distances_normal = switch_distances_sum[0]
                distances_switched = np.min(switch_distances_sum)

                if np.argmin(switch_distances_sum) != 0:
                    if distances_switched * 1.5 < distances_normal:
                        print(
                            f"Switch: {connections} distance sum\t{np.min(switch_distances_sum):.2f} vs\t{np.min(switch_distances_sum[0]):.2f}"
                        )
                        poses[t:] = poses[t:, connections]
                        all_switches.append(t)

        # Update last poses for every fish that is not nan
        last_poses[np.where(~np.isnan(poses[t]))] = poses[t][
            np.where(~np.isnan(poses[t]))
        ]

        assert not np.any(np.isnan(last_poses)), "Error: NaN in last_poses"

    return poses, all_switches


def handle_file(file, args):
    pf = pd.read_csv(file, skiprows=4, header=None, sep=args.sep)
    assert (
        len(pf.columns) >= 2
    ), f"{pf}\nThere were only {len(pf.columns)} columns in {file}. Looks like the seperator was maybe wrong? Choose it with --sep. Default is ;"

    if args.columns_per_entity is None:
        all_col_types_matching = []
        for cols in range(1, len(pf.columns) // 2 + 1):
            dt = np.array(pf.dtypes, dtype=str)
            # print(dt)

            extraced_col_types = np.array(
                [
                    dt[len(dt) - (cols * (i + 1)) : len(dt) - (cols * i)]
                    for i in range(len(dt) // cols)
                ]
            )

            matching_types = [
                all(extraced_col_types[i] == extraced_col_types[0])
                for i in range(len(extraced_col_types))
            ]

            if all(matching_types):
                all_col_types_matching.append(cols)
            # print(cols, "\t", matching_types)

        assert (
            len(all_col_types_matching) > 0
        ), f"Error: Could not detect columns_per_entity. Please specify manually using --columns_per_entity\ndatatypes were {dt}"
        assert [
            all_col_types_matching[i] % all_col_types_matching[0] == 0
            for i in range(0, len(all_col_types_matching))
        ], f"Error: Found multiple columns_per_entity which were not multiples of each other {all_col_types_matching}"
        columns_per_entity = all_col_types_matching[0]
        print("Found columns_per_entity: %d" % columns_per_entity)
    else:
        columns_per_entity = int(args.columns_per_entity)

    n_fish = len(pf.columns) // columns_per_entity
    header_cols = len(pf.columns) % n_fish

    print(
        f"Header columns: {header_cols}, n_fish: {n_fish}, columns per fish: {columns_per_entity} total columns: {len(pf.columns)}"
    )
    print()

    print(
        f"IMPORTANT: Check if this is correct (not automatic):\n\tColumn {args.xcol} is selected as x coordinate\n\tColumn {args.ycol} is selected as y coordinate.\n\tColumn {args.oricol} is selected as orientation.\nIf this is not correct, please specify the correct columns using --xcol, --ycol and --oricol."
    )
    print()

    first_fish = pf.loc[:, header_cols : header_cols + columns_per_entity - 1].head()
    first_fish.columns = range(columns_per_entity)
    print(first_fish)

    pf_np = pf.to_numpy()
    print(pf_np.shape)

    io_file_path = str(file)[:-4] + ".hdf5" if args.output is None else args.output

    with robofish.io.File(
        io_file_path,
        "w",
        world_size_cm=[args.world_size, args.world_size],
        world_shape=args.world_shape,
        frequency_hz=args.frequency,
    ) as iof:
        poses = np.empty((pf_np.shape[0], n_fish, 3), dtype=np.float32)

        for f in range(n_fish):

            f_cols = pf_np[
                :,
                header_cols
                + f * columns_per_entity : header_cols
                + (f + 1) * columns_per_entity,
            ]
            poses[:, f] = f_cols[
                :, [int(args.xcol), int(args.ycol), int(args.oricol)]
            ].astype(np.float32)

        if not args.disable_centering:
            poses[:, :, :2] -= args.world_size / 2  #  center world around 0,0
            poses[:, :, 1] *= -1  #                    flip y axis
            poses[:, :, 2] *= 2 * np.pi / 360  #       convert to radians
            poses[:, :, 2] -= np.pi  #                 rotate 180 degrees

        supress = []

        all_switches = None
        if not args.disable_fix_switches:
            for run in range(20):
                print("RUN ", run)
                switched_poses, all_switches = handle_switches(poses)
                print("All switches: ", all_switches)
                diff = np.diff(all_switches, axis=0)

                new_supress = np.array(all_switches)[
                    np.where(diff < args.min_timesteps_between_switches)
                ]
                new_supress = [n for n in new_supress if n not in supress]

                if len(new_supress) == 0:
                    print("No need to supress more.")
                    break

                supress.extend(new_supress)
                print("supressing in the next runn: ", supress)

            # distances = get_distances(switched_poses)
            # switched_poses[
            #    np.where(np.diagonal(distances > 1, axis1=1, axis2=2))
            # ] = np.nan
            poses = switched_poses

        for f in range(n_fish):
            iof.create_entity("fish", poses[:, f])
        if all_switches is not None:
            iof.attrs["switches"] = all_switches

        # assert np.all(
        #     poses[np.logical_not(np.isnan(poses[:, 0])), 0] >= 0
        # ), f"Error: x coordinate is not positive, {np.min(poses[:, 0])}"
        # assert (poses[:, 1] >= -1).all(), "Error: y coordinate is not positive"
        # assert (poses[:, 0] <= 101).all(), "Error: x coordinate is not <= 100"
        # assert (poses[:, 1] <= 101).all(), "Error: y coordinate is not <= 100"
        # assert (poses[:, 2] >= 0).all(), "Error: orientation is not >= 0"
        # assert (poses[:, 2] <= 2 * np.pi).all(), "Error: orientation is not 2*pi"
    return io_file_path, all_switches


def eliminate_flips(io_file_path, analysis_path) -> None:
    """This function eliminates flips in the orientation of the fish.

    First we check if we find a pair of two close flips with low speed. If we do we flip the fish between these two flips.

    Args:
        io_file_path (str): The path to the hdf5 file that should be corrected.
    """

    if analysis_path is not None:
        analysis_path = Path(analysis_path)
        if analysis_path.exists() and analysis_path.is_dir():
            analysis_path = analysis_path / "flip_analysis.png"

    print("Eliminating flips in ", io_file_path)

    flipps = []
    flipp_starts = []

    with robofish.io.File(io_file_path, "r+") as iof:
        n_timesteps = iof.entity_actions_speeds_turns.shape[1]

        fig, ax = plt.subplots(1, len(iof.entities), figsize=(20, 7))

        for e, entity in enumerate(iof.entities):
            actions = entity.actions_speeds_turns

            turns = actions[:, 1]

            biggest_turns = np.argsort(np.abs(turns[~np.isnan(turns)]))[::-1]

            flips = {}

            for investigate_turn in biggest_turns:
                if (
                    investigate_turn not in flips.keys()
                    and investigate_turn not in flips.values()
                    and not np.isnan(turns[investigate_turn])
                    and np.abs(turns[investigate_turn]) > 0.6 * np.pi
                ):
                    # Find the biggest flip within 10 timesteps from investigate_turn
                    turns_below = turns[investigate_turn - 20 : investigate_turn]
                    turns_above = turns[investigate_turn + 1 : investigate_turn + 20]

                    turns_wo_investigated = np.concatenate(
                        [turns_below, [0], turns_above]
                    )
                    turns_wo_investigated[np.isnan(turns_wo_investigated)] = 0

                    biggest_neighbors = np.argsort(np.abs(turns_wo_investigated))[::-1]

                    for neighbor in biggest_neighbors:
                        if (
                            neighbor not in flips.keys()
                            and neighbor not in flips.values()
                        ):
                            if np.abs(turns_wo_investigated[neighbor]) > 0.6 * np.pi:
                                flips[investigate_turn] = neighbor + (
                                    investigate_turn - 10
                                )
                                break

            for k, v in flips.items():
                start = min(k, v)
                end = max(k, v)

                entity["orientations"][start:end] = (
                    np.pi - entity["orientations"][start:end]
                ) % (np.pi * 2)

                print(f"Flipping from {start} to {end}")

                flipps.extend(list(range(start, end)))
                flipp_starts.extend([start])

            if analysis_path is not None:
                all_flip_idx = np.array(list(flips.values()) + list(flips.keys()))

                # Transfer the flip ids to the sorted order of abs turns
                turn_order = np.argsort(np.abs(turns))

                x = np.arange(len(turns), dtype=np.int32)

                ax[e].scatter(
                    x,
                    np.abs(turns[turn_order]) / np.pi,
                    c=[
                        "blue" if i not in all_flip_idx else "red"
                        for i in x[turn_order]
                    ],
                    alpha=0.5,
                )

        if analysis_path is not None:
            plt.savefig(analysis_path)

        flipps = [i for i in range(n_timesteps) if i in flipps]

        iof.attrs["switches"] = np.array(flipps, dtype=np.int32)

        iof.update_calculated_data()

    return flipp_starts


parser = argparse.ArgumentParser(
    description="This tool converts files from csv files. The column names come currently from 2019/Q_trials."
)
parser.add_argument("path", nargs="+")
parser.add_argument("-o", "--output", default=None)
parser.add_argument("--world_shape", type=str)
parser.add_argument("--sep", default=";")
parser.add_argument("--header", default=3)
parser.add_argument("--columns_per_entity", default=None)
parser.add_argument("--xcol", default=4)
parser.add_argument("--ycol", default=5)
parser.add_argument("--oricol", default=7)
parser.add_argument("--world_size", default=100)
parser.add_argument("--frequency", default=25)
parser.add_argument("--disable_fix_switches", action="store_true")
parser.add_argument("--disable_fix_flips", action="store_true")
parser.add_argument("--disable_centering", action="store_true")
parser.add_argument("--min_timesteps_between_switches", type=int, default=0)
parser.add_argument(
    "--analysis_path",
    default=None,
    help="Path to save analysis to. Folder will create two png files.",
)
args = parser.parse_args()

assert args.world_shape in [
    "rectangle",
    "ellipse",
], f"--world_shape has to be 'rectangle' or 'ellipse' but is '{args.world_shape}'"

if args.analysis_path is not None:
    args.analysis_path = Path(args.analysis_path)

for path in args.path:
    path = Path(path)

    if not path.exists():
        print("Path '%s' not found" % path)
        continue

    if path.suffix == ".csv":
        files = [path]

    elif path.is_dir():
        files = path.rglob("*.csv")
    else:
        print("'%s' is not a folder nor a csv file" % path)
        continue

    for file in tqdm(files):
        io_file_path, all_switches = handle_file(file, args)
        if not args.disable_fix_flips:
            all_flipps = eliminate_flips(io_file_path, args.analysis_path)

        if args.analysis_path is not None and args.analysis_path.is_dir():
            plt.figure()
            plt.hist([all_switches, all_flipps], bins=50, label=["switches", "flips"])
            plt.legend()
            plt.savefig(args.analysis_path / "switches_flipps.png")
