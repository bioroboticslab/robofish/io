# -*- coding: utf-8 -*-

"""
This script converts datasets from M.Maxeiners Masterthesis to the trackformat.

The original datasets are available at
https://zenodo.org/record/3457834#.X9eC8MIo9FE

The script is not maintained, since Moritz' files were all converted to io files.
"""

# Dec 2020 Andreas Gerken, Berlin, Germany
# Released under GNU 3.0 License
# email andi.gerken@gmail.com

# flake8: noqa

from pathlib import Path
import robofish.io
import h5py
import numpy as np
import argparse

try:
    from tqdm import tqdm
except ImportError:
    print("tqdm not installed, not showing progress bar.")

    def tqdm(x):
        return x


def format_mm_data_folder(input, output):
    input, output = Path(input), Path(output)

    files = input.rglob("*.hdf5")
    files = (list)(files)
    for f in tqdm(files):
        # print("Handling file %s" % f.relative_to(input))

        old = h5py.File(f)
        world = old.attrs["world"] if "world" in old.attrs else [100, 100]
        feq = 1000 / old.attrs["time_steps"] if "time_steps" in old.attrs else 25
        new = robofish.io.File(world_size_cm=world, frequency_hz=feq)

        new.attrs[
            "experiment_setup"
        ] = "The Data was generated in Moritz Maxeiners Masterthesis (https://www.mi.fu-berlin.de/inf/groups/ag-ki/Theses/Completed-theses/Master_Diploma-theses/2019/Maxeiner/MA-Maxeiner2.pdf)."
        if "couzin_torus" in str(f.resolve()):
            new.attrs[
                "experiment_setup"
            ] += " The couzin torus model was used to generate the data"
            new.attrs[
                "experiment_config"
            ] = '<couzin><zones><repulsion>1</repulsion><orientation>4</orientation><attraction>20</attraction>  <wallrepulsion>20</wallrepulsion></zones><field_of_perception unit="degree">300</field_of_perception><turn_rate unit="degree">45</turn_rate><speed>8</speed><direction_error unit="degree">0</direction_error></couzin>'

        new.attrs.update(
            {
                a: old.attrs[a]
                for a in old.attrs.keys()
                if a not in ["world", "time_step"]
            }
        )

        for e_name, e_data in old.items():
            poses = np.array(e_data)

            # The original coordinate system had the origin in the top left and the
            # y axis pointing "down". We transform it

            # Move x axis, to be centered
            poses[:, 0] -= world[0] / 2
            # Invert and move y axis (world_y - pose_y) - world_y / 2
            poses[:, 1] = world[1] / 2 - poses[:, 1]
            # Invert the y axis of the rotation vector
            poses[:, 3] *= -1

            fish = new.create_entity(category="fish", name=e_name, poses=poses)
            if "live_female_female" in str(f.resolve()):
                fish.attrs["sex"] = "female"
                fish.attrs["species"] = "guppy"
        if not new.validate(strict_validate=False)[0]:
            print(
                f"File {input.relative_to(input)} could not be converted to a valid file. The error was:\n {new.validate(strict_validate=False)[1]}."
            )
            continue

        new.save(output / f.relative_to(input))
        # print("Saved converted file")


if __name__ == "__main__":
    # Please configure to your local folder here

    parser = argparse.ArgumentParser(
        description="This function can be used to convert hdf5 moritz' hdf5 files"
    )

    parser.add_argument("-in", "--in_folder", type=str)
    parser.add_argument("-out", "--out_folder", type=str)

    args = parser.parse_args()
    # mm_data_folder = "/home/andi/phd_workspace/mm/mm"

    format_mm_data_folder(args.in_folder, args.out_folder)
