import robofish.io
import numpy as np
import argparse
import itertools
import shutil
from pathlib import Path


def get_distances(poses, last_poses=None, diagonal=False):
    def get_single_distance(pose_1, pose_2):
        assert pose_1.shape == pose_2.shape == (3,)
        return np.linalg.norm(pose_1[:2] - pose_2[:2])

    if poses.ndim == 2:
        n_fish, three = poses.shape
        distances = np.zeros((n_fish, n_fish))
        for i in range(n_fish):
            for j in range(n_fish):
                if i == j or not diagonal:
                    if last_poses is None:
                        distances[i, j] = get_single_distance(poses[i], poses[j])
                    else:
                        distances[i, j] = get_single_distance(poses[i], last_poses[j])
    else:
        n_frames, n_fish, three = poses.shape
        distances = np.zeros((n_frames, n_fish, n_fish))

        for t in range(n_frames - 1):
            for i in range(n_fish):
                for j in range(n_fish):
                    if i == j or not diagonal:
                        assert last_poses is None
                        distances[t, i, j] = get_single_distance(
                            poses[t + 1, i], poses[t, j]
                        )
    if diagonal:
        return np.diagonal(distances, axis1=-1, axis2=-2)
    else:
        return distances


def handle_switches(poses, supress=None, verbose=True):
    """Find and handle switches in the data.
    Switches are defined as a fish that is not at the same position as it was in the previous frame.
    If a switch is found, the fish is moved to the position of the fish that was at the same position in the previous frame.
    If multiple switches are found, the fish that is closest to the position of the fish that was at the same position in the previous frame is moved.

    Args:
        poses (np.ndarray): Array of shape (n_frames, n_fish, 3) containing the poses of the fish.
        supress (list): List of frames to ignore.
    Returns:
        np.ndarray: Array of shape (n_frames, n_fish, 3) containing the poses of the fish with switches handled.
    """

    n_timesteps, n_fish, three = poses.shape

    all_switches = []

    last_poses = np.copy(poses[0])

    all_connection_permutations = list(itertools.permutations(np.arange(n_fish)))

    for t in range(1, poses.shape[0]):
        if np.all(np.isclose(np.abs(np.diff(poses[t - 1], axis=0)), 0)):
            if verbose:
                print(
                    f"Warning: All fish at same position in frame {t - 1} setting them to NaN"
                )
            poses[t - 1] = np.nan

        if supress is None or t not in supress:
            distances = get_distances(poses[t], last_poses)

            switches = {}
            for i in range(n_fish):
                if np.argmin(distances[i]) != i:
                    switches[i] = np.argmin(distances[i])

            if len(switches) > 0 or np.any(
                np.isnan(poses[t]) != np.isnan(poses[t - 1])
            ):
                switch_distances = np.array(
                    [
                        get_distances(poses[t, con_perm], last_poses, diagonal=True)
                        for con_perm in all_connection_permutations
                    ]
                )

                switch_distances_sum = np.nansum(switch_distances, axis=1)

                # if t > 10 and t < 18:
                #    print("Poses\n", poses[t], "\nLast Poses\n", last_poses)
                #    print(t, "\n", switch_distances[:2], "\n", switch_distances_sum[:2])

                connections = np.array(
                    all_connection_permutations[np.argmin(switch_distances_sum)]
                ).astype(int)

                distances_normal = switch_distances_sum[0]
                distances_switched = np.min(switch_distances_sum)

                if np.argmin(switch_distances_sum) != 0:
                    if distances_switched * 1.5 < distances_normal:
                        if verbose:
                            print(
                                f"Switch: {connections} distance sum\t{np.min(switch_distances_sum):.2f} vs\t{np.min(switch_distances_sum[0]):.2f}"
                            )
                        poses[t:] = poses[t:, connections]
                        all_switches.append(t)

        # Update last poses for every fish that is not nan
        last_poses[np.where(~np.isnan(poses[t]))] = poses[t][
            np.where(~np.isnan(poses[t]))
        ]

        assert not np.any(np.isnan(last_poses)), "Error: NaN in last_poses"
    return poses, all_switches


def handle_single_file(file_path, out_file_path, v=True):
    # Load file
    print("Loading file...")
    with robofish.io.File(file_path) as data:
        swapped_poses = np.swapaxes(data.entity_poses_rad, 0, 1)

    print("Handling switches...")
    fixed_poses, switches = handle_switches(swapped_poses, verbose=v)

    print(f"Found {len(switches)} switches and fixed them.")

    # Save file
    print("Saving file...")

    # copy file from file to out file
    shutil.copyfile(file_path, out_file_path)

    with robofish.io.File(out_file_path, "r+") as f:
        for e, entity in enumerate(f.entities):
            entity.positions[:] = fixed_poses[:, e, :2]

            # Orientations are converted into vectors
            entity.orientations[:] = np.stack(
                [np.cos(fixed_poses[:, e, 2]), np.sin(fixed_poses[:, e, 2])], axis=-1
            )
        # data.entity_poses_rad = np.swapaxes(poses, 0, 1)


def app_fix_switches():
    # Get file from args
    parser = argparse.ArgumentParser()
    parser.add_argument("file", help="File to fix")
    parser.add_argument("out_file", help="File to save to")
    # verbosity argument
    parser.add_argument("-v", action="store_true")
    parser.add_argument(
        "--min_timesteps_between_switches",
        type=int,
        default=10,
        help="Minimum number of timesteps between switches",
    )
    args = parser.parse_args()

    file, out_file = Path(args.file), Path(args.out_file)

    if file.is_dir():
        if not out_file.exists() or not out_file.is_dir():
            raise ValueError(
                f"Output file {out_file} must be a directory and must exist"
            )
        for f in file.glob("*.hdf5"):
            print(f"Handling file {f}")
            handle_single_file(f, out_file / f.name, v=args.v)
    else:
        handle_single_file(file, out_file, v=args.v)


if __name__ == "__main__":
    app_fix_switches()
