# SPDX-License-Identifier: LGPL-3.0-or-later

"""
The Python package <a href="https://git.imp.fu-berlin.de/bioroboticslab/robofish/io">robofish.io</a> provides a simple interface to create, load, modify, and inspect files containing world information and movement tracks of entities (organisms, robots, obstacles,...).
The files are saved in the `.hdf5` format and following the [Track Format Specification](https://git.imp.fu-berlin.de/bioroboticslab/robofish/track_format/uploads/f76d86e7a629ca38f472b8f23234dbb4/RoboFish_Track_Format_-_1.0.pdf).

.. include:: ../../../docs/index.md
"""

import sys
import logging
import yaml

from robofish.io.file import *
from robofish.io.entity import *
from robofish.io.validation import *
from robofish.io.io import *
from robofish.io.utils import *
from robofish.io.fix_switches import *


if not ((3, 7) <= sys.version_info < (4, 0)):
    logging.warning("Unsupported Python version")


def user_config(overwrite=True) -> dict:
    """Returns the user config.

    Returns:
        dict: Dict with all user settings.
    """

    p = Path.home() / ".robofish/io" / "config.yaml"

    print("Reading user config from", p)
    if not p.exists() or overwrite:
        logging.warning(
            "User config not found (or overwrite). Creating file and searching for fish_models."
        )
        p.parent.mkdir(parents=True, exist_ok=True)
        p.touch()
        import imp

        fm = imp.find_module("fish_models")[1]
        p.write_text(yaml.dump({"fish_models_path": str(Path(fm).parents[1])}))

    # Load config
    with open(p, "r") as f:
        config = yaml.safe_load(f)

        if config is None:
            config = {}

    assert (
        "fish_models_path" in config
    ), "fish_models_path: Please add the path to the fish_models package to the config file. Update the file automatically with the bash command `roborish-io-overwrite_user_configs`."

    assert Path(
        config["fish_models_path"]
    ).exists(), f"fish_models_path: The path to the fish_models package does not exist. {config['fish_models_path']}. Update the file automatically with the bash command `roborish-io-overwrite_user_configs`."
    return config
