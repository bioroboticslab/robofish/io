# -*- coding: utf-8 -*-

"""
.. include:: ../../../docs/app.md
"""

# -----------------------------------------------------------
# Dec 2020 Andreas Gerken, Berlin, Germany
# Released under GNU 3.0 License
# email andi.gerken@gmail.com
# -----------------------------------------------------------

import robofish.io
from robofish.io import utils

import argparse
import logging
import warnings
from tqdm.auto import tqdm

from typing import Dict
import itertools
import numpy as np
import multiprocessing
import copy
from pathlib import Path


def print_file(args: argparse.Namespace = None) -> bool:
    """This function can be used to print hdf5 files from the command line

    Args:
        args (argparse.Namespace, optional): The arguments for the render function. This is mainly used for testing.

    Returns:
        bool: A boolean if the file was invalid [True if invalid, False if not]
    """
    parser = argparse.ArgumentParser(
        description="This function can be used to print hdf5 files from the command line"
    )

    parser.add_argument("path", type=str, help="The path to a hdf5 file")
    parser.add_argument(
        "--output_format",
        type=str,
        choices=["shape", "full"],
        default="shape",
        help="Choose how datasets are printed, either the shapes or the full content is printed",
    )
    parser.add_argument(
        "--full_attrs",
        default=False,
        action="store_true",
        help="Show full unabbreviated values for attributes",
    )

    if args is None:
        args = parser.parse_args()

    with robofish.io.File(path=args.path, strict_validate=False) as f:
        print(f.to_string(output_format=args.output_format, full_attrs=args.full_attrs))
        print()
        valid = f.validate(strict_validate=False)[0]
        print("Valid file" if valid else "Invalid file")
    return not valid


def update_calculated_data(args: argparse.Namespace = None) -> None:
    """This function updates the calculated data of any number of files or folders.

    Args:
        args (argparse.Namespace, optional): The arguments for the render function. This is mainly used for testing.
    """
    parser = argparse.ArgumentParser(
        description="This function updates all calculated data from files."
    )

    parser.add_argument(
        "path",
        type=str,
        nargs="+",
        help="The path to one or multiple files and/or folders.",
    )
    if args is None:
        args = parser.parse_args()

    files_per_path = utils.get_all_files_from_paths(args.path)
    files = [
        f for f_in_path in files_per_path for f in f_in_path
    ]  # Concatenate all files to one list

    assert len(files) > 0, f"No files found in path {args.path}."

    pbar = tqdm(files)
    for fp in pbar:

        try:
            with robofish.io.File(fp, "r+", validate_poses_hash=False) as f:
                if f.update_calculated_data(verbose=False):
                    pbar.set_description(f"File {fp} was updated")
                else:
                    pbar.set_description(f"File {fp} was already up to date")
        except Exception as e:
            warnings.warn(f"The file {fp} could not be updated.")
            print(e)


def validate(args: argparse.Namespace = None) -> int:
    """This function can be used to validate hdf5 files.

    The function can be directly accessed from the commandline and can be given
    any number of files or folders. The function returns the validity of the files
    in a human readable format or as a raw output.

    Args:
        args (argparse.Namespace, optional): The arguments for the render function. This is mainly used for testing.

    Returns:
        int: An error code. 0 if all files are valid, 1 if at least one file is invalid.
    """
    parser = argparse.ArgumentParser(
        description="The function can be directly accessed from the commandline and can be given any number of files or folders. The function returns the validity of the files in a human readable format or as a raw output."
    )
    parser.add_argument(
        "--output_format",
        type=str,
        default="h",
        choices=["h", "raw"],
        help="Output format, can be either h for human readable or raw for a dict.",
    )
    parser.add_argument(
        "path",
        type=str,
        nargs="+",
        help="The path to one or multiple files and/or folders.",
    )

    if args is None:
        args = parser.parse_args()

    logging.getLogger().setLevel(logging.ERROR)

    files_per_path = utils.get_all_files_from_paths(args.path)
    files = [
        f for f_in_path in files_per_path for f in f_in_path
    ]  # Concatenate all files to one list

    if len(files) == 0:
        logging.getLogger().setLevel(logging.INFO)
        logging.info("No files found in %s" % args.path)
        return

    validity_dict = {}
    for fp in files:
        with robofish.io.File(fp) as f:
            validity_dict[str(fp)] = f.validate(strict_validate=False)

    if args.output_format == "raw":
        return validity_dict

    max_filename_width = max([len(str(f)) for f in files])
    error_code = 0
    for fp, (validity, validity_message) in validity_dict.items():

        filled_file = (str)(fp).ljust(max_filename_width + 3)

        if not validity:
            error_code = 1
        msg = f"{filled_file}:{validity}"
        if validity_message != "":
            msg += f"\n{validity_message}"
        print(msg)
    return error_code


def render_file(kwargs: Dict) -> None:
    """This function renders a single file.
    Args:
        kwargs (Dict, optional): A dictionary containing the arguments for the render function.
    """
    with robofish.io.File(path=kwargs["path"]) as f:
        f.render(**kwargs)


def overwrite_user_configs() -> None:
    """This function overwrites the user configs with the default config."""
    robofish.io.user_config(overwrite=True)


def render(args: argparse.Namespace = None) -> None:
    """This function can be used to render hdf5 files.

    The function can be directly accessed from the commandline and can be given
    any number of files.

    Args:
        args (argparse.Namespace, optional): The arguments for the render function. This is mainly used for testing.
    """

    parser = argparse.ArgumentParser(
        description="This function shows the file as animation."
    )
    parser.add_argument(
        "path",
        type=str,
        nargs="+",
        help="The path to one file.",
    )

    parser.add_argument(
        "-vp",
        "--video_path",
        default=None,
        type=str,
        help="Path to save the video to (mp4). If a path is given, the animation won't be played.",
    )

    parser.add_argument(
        "--reference_track",
        action="store_true",
        help="If true, the reference track will be rendered in parallel.",
        default=False,
    )

    default_options = {
        "linewidth": 2,
        "speedup": 1,
        "trail": 100,
        "entity_scale": 0.2,
        "fixed_view": False,
        "view_size": 60,
        "slow_view": 0.8,
        "cut_frames_start": 0,
        "cut_frames_end": 0,
        "show_text": False,
        "show_ids": False,
        "render_goals": False,
        "render_targets": False,
        "highlight_switches": False,
        "figsize": 10,
    }

    for key, value in default_options.items():
        if isinstance(value, bool):
            parser.add_argument(
                f"--{key}",
                default=value,
                action="store_true" if value is False else "store_false",
                help=f"Optional setter for video option {key}.\tDefault: {value}",
            )
        else:
            parser.add_argument(
                f"--{key}",
                default=value,
                type=type(value),
                help=f"Optional video option {key} with type {type(value)}.\tDefault: {value}",
            )

    if args is None:
        args = parser.parse_args()

    if args.reference_track:
        assert (
            len(args.path) == 1
        ), "Only one file can be rendered with the reference track."

        # Load fish_models path from user_config.yaml
        user_config = robofish.io.user_config()

        with robofish.io.File(args.path[0]) as f:

            data_folder = f["initial_poses_info"].attrs["preload_data"]
            reference_track = f["initial_poses_info"].attrs["file_name"]

            reference_track_path = (
                Path(user_config["fish_models_path"])
                / "storage/raw_data"
                / data_folder
                / reference_track
            )

            args.path.append(str(reference_track_path))

    if len(args.path) > 1:
        print("Found multiple paths, starting multiprocessing.")
        args_array = []
        for v in args.path:
            kwargs = copy.copy(vars(args))
            kwargs["path"] = v
            args_array.append(kwargs)

        # Render multiple animations with multiprocessing
        with multiprocessing.Pool() as pool:
            pool.map(render_file, args_array)
    else:
        kwargs = vars(args)
        kwargs["path"] = args.path[0]
        render_file(kwargs)


def update_individual_ids(args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "path",
        type=str,
        nargs="+",
        help="The path to one or multiple files and/or folders.",
    )
    parser.add_argument(
        "-o",
        "--offset",
        type=int,
        default=0,
        help="The offset for the first individual ids.",
    )

    if args is None:
        args = parser.parse_args()

    files_per_path = utils.get_all_files_from_paths(args.path)
    files_per_path = [sorted(f_in_path) for f_in_path in files_per_path]
    files = list(itertools.chain.from_iterable(files_per_path))
    file_names = [f.name for f in files]

    # make sure that there are no duplicate files
    unique, counts = np.unique(file_names, return_counts=True)
    if not (counts == 1).all():
        warnings.warn(
            f"There are duplicate files in the path! {unique[np.where(counts != 1)]}. \nThey will get the same individual ids."
        )

    y = input(
        f"Are you sure you want to update the global individual id of {len(files)} files? (y/n)"
    )
    if y != "y":
        print("The update was aborted. Printing the existing individual ids.")
    else:

        # Split filenames like "recording1-sub_0" into ["recording1", "9"]
        split_file_names = np.array([str(f.name).split("-sub_") for f in files])

        # Find uniques in the first part of the split names
        _, unique_inverse = np.unique(split_file_names[:, 0], return_inverse=True)

        running_individual_id = args.offset

        for unique in range(max(unique_inverse) + 1):
            file_ids = np.where(unique_inverse == unique)

            for file in np.array(files)[file_ids]:

                video = None
                n_fish = None

                with robofish.io.File(file, "r+") as f:
                    if "initial_poses_info" in f:
                        print(f"Found initial_poses_info in {file}.")
                        for e, entity in enumerate(f.entities):
                            entity.attrs["individual_id"] = int(
                                f["initial_poses_info"].attrs["individual_ids"][e]
                            )

                        running_individual_id = None
                    else:
                        assert (
                            running_individual_id is not None
                        ), "The update script found files with initial_poses_info and files without. Mixing them is not supported."
                        if n_fish is None:
                            n_fish = len(f.entities)
                        else:
                            assert n_fish == len(
                                f.entities
                            ), f"Number of fish in file {file} is not the same as in the previous file."
                        if "video" in f.attrs:
                            if video is None and "video" in f.attrs:
                                video = f.attrs["video"]
                            else:
                                assert (
                                    video == f.attrs["video"]
                                ), f"Video in file {file} is not the same as in the previous file."

                        for e, entity in enumerate(f.entities):

                            entity.attrs["individual_id"] = running_individual_id + e

                            # Delete the old individual_id attribute
                            if "global_individual_id" in entity.attrs:
                                del entity.attrs["global_individual_id"]

                        running_individual_id += n_fish
        print("Update finished.")

    for fp in sorted(files):
        with robofish.io.File(fp, "r") as f:
            try:
                print(f"File {fp}\t: {[e.attrs['individual_id'] for e in f.entities]}")
            except Exception as e:
                if y == "y":
                    print(f"Could not read individual_id from {fp}")
                    print(e)


def update_world_shape(args: dict = None) -> None:
    """Update the world shape attribute of files.

    Args:
        args (dict): Passthrough for argparser args. Defaults to None.
    """
    if args is None:
        parser = argparse.ArgumentParser()
        parser.add_argument(
            "path",
            type=str,
            nargs="+",
            help="The path to one or multiple files and/or folders.",
        )
        parser.add_argument("world_shape", type=str, help="The world shape.")
        args = parser.parse_args()

    assert args.world_shape in [
        "rectangle",
        "ellipse",
    ], "The world shape must be either 'rectangle' or 'ellipse'."

    files_per_path = utils.get_all_files_from_paths(args.path)
    files_per_path = [sorted(f_in_path) for f_in_path in files_per_path]
    files = list(itertools.chain.from_iterable(files_per_path))

    pbar = tqdm(files)
    for file in pbar:

        pbar.set_description(f"Updating {file.name}")
        with robofish.io.File(
            file, "r+", validate_when_saving=False, calculate_data_on_close=False
        ) as f:
            f.attrs["world_shape"] = args.world_shape

        pbar.update(1)

    print("Update finished.")
