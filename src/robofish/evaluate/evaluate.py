# -*- coding: utf-8 -*-

"""Evaluation functions, to generate graphs from files."""

# Feb 2022 Andreas Gerken Berlin, Germany
# Released under GNU 3.0 License
# email andi.gerken@gmail.com

import robofish.io
import robofish.evaluate
from robofish.io import utils

from pathlib import Path

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
import numpy as np
import pandas as pd
from typing import Iterable, Union, Callable, List
from scipy import stats
from tqdm import tqdm
import inspect
import random
import warnings


def evaluate_speed(
    paths: Iterable[Union[str, Path]],
    labels: Iterable[str] = None,
    predicate: Callable[[robofish.io.Entity], bool] = None,
    speeds_turns_from_paths: Iterable[Iterable[np.ndarray]] = None,
) -> matplotlib.figure.Figure:
    """Evaluate the speed of the entities as histogram.

    Lambda function example: lambda e: e.category == "fish"

    Args:
        paths(Iterable[Union[str, Path]]): An array of strings, with files of folders.
        labels(Iterable[str]): Labels for the paths. If no labels are given, the paths will be used.
        predicate(Callable[[robofish.io.Entity], bool]): a lambda function, selecting entities
        speeds_turns_from_paths(Iterable[Iterable[np.ndarray]]): An array of speeds for each path.
    Returns:
        matplotlib.figure.Figure: The figure of the histogram.
    """

    if speeds_turns_from_paths is None:
        speeds_turns_from_paths, _ = utils.get_all_data_from_paths(
            paths, "speeds_turns", predicate=predicate
        )

    speeds = []

    # Iterate all paths
    for speeds_turns_per_path in speeds_turns_from_paths:
        path_speeds = []
        left_quantiles, right_quantiles = [], []

        # Iterate all files
        for speeds_turns in speeds_turns_per_path:
            for e_speeds_turns in speeds_turns:
                path_speeds.extend(e_speeds_turns[:, 0])

        # Exclude possible nans
        path_speeds = np.array(path_speeds)

        left_quantiles.append(np.quantile(path_speeds, 0.001))
        right_quantiles.append(np.quantile(path_speeds, 0.999))
        speeds.append(path_speeds)

    fig = plt.figure()
    plt.hist(
        list(speeds),
        bins=30,
        label=labels,
        density=True,
        range=[min(left_quantiles), max(right_quantiles)],
    )
    plt.title("Agent speeds")
    plt.xlabel("Speed [cm/s]")
    plt.ylabel("Frequency")
    plt.ticklabel_format(useOffset=False, style="plain")
    plt.legend()
    plt.tight_layout()
    return fig


def evaluate_turn(
    paths: Iterable[Union[str, Path]],
    labels: Iterable[str] = None,
    predicate: Callable[[robofish.io.Entity], bool] = None,
    speeds_turns_from_paths: Iterable[Iterable[np.ndarray]] = None,
    file_settings: dict = None,
) -> matplotlib.figure.Figure:
    """Evaluate the turn angles of the entities as histogram.

    Lambda function example: lambda e: e.category == "fish"

    Args:
        paths(Iterable[Union[str, Path]]): An array of strings, with files of folders.
        labels(Iterable[str]): Labels for the paths. If no labels are given, the paths will be used.
        predicate(Callable[[robofish.io.Entity], bool]): a lambda function, selecting entities
        speeds_turns_from_paths(Iterable[Iterable[np.ndarray]]): An array of speeds for each path.
        file_settings(dict): Settings for the files.
    Returns:
        matplotlib.figure.Figure: The figure of the histogram.
    """

    if speeds_turns_from_paths is None:
        speeds_turns_from_paths, file_settings = utils.get_all_data_from_paths(
            paths, "speeds_turns", predicate=predicate
        )

    turns = []

    # Iterate all paths
    for speeds_turns_per_path in speeds_turns_from_paths:
        path_turns = []
        left_quantiles, right_quantiles = [], []

        # Iterate all files
        for speeds_turns in speeds_turns_per_path:
            for e_speeds_turns in speeds_turns:
                path_turns.extend(np.rad2deg(e_speeds_turns[:, 1]))

        path_turns = np.array(path_turns)

        left_quantiles.append(np.quantile(path_turns, 0.001))
        right_quantiles.append(np.quantile(path_turns, 0.999))
        turns.append(path_turns)

    fig = plt.figure()
    plt.hist(
        turns,
        bins=41,
        label=labels,
        density=True,
        range=[min(left_quantiles), max(right_quantiles)],
    )
    plt.title("Agent turns")
    plt.xlabel(
        f"Change in orientation [Degree / timestep at {file_settings['frequency_hz']} hz"
    )
    plt.ylabel("Frequency")
    plt.ticklabel_format(useOffset=False, style="plain")
    plt.legend()
    # plt.tight_layout()

    return fig


def evaluate_orientation(
    paths: Iterable[Union[str, Path]],
    labels: Iterable[str] = None,
    predicate: Callable[[robofish.io.Entity], bool] = None,
    poses_from_paths: Iterable[Iterable[np.ndarray]] = None,
    file_settings: dict = None,
) -> matplotlib.figure.Figure:
    """Evaluate the orientations of the entities on a 2d grid.

    Lambda function example: lambda e: e.category == "fish"

    Args:
        paths(Iterable[Union[str, Path]]): An array of strings, with files of folders.
        labels(Iterable[str]): Labels for the paths. If no labels are given, the paths will be used.
        predicate(Callable[[robofish.io.Entity], bool]): a lambda function, selecting entities
        poses_from_paths(Iterable[Iterable[np.ndarray]]): An array of poses for each path.
        file_settings(dict): Settings for the files.
    Returns:
        matplotlib.figure.Figure: The figure of the histogram.
    """
    if poses_from_paths is None:
        poses_from_paths, file_settings = utils.get_all_poses_from_paths(
            paths, predicate=predicate
        )

    world_bounds = [
        -file_settings["world_size_cm_x"] / 2,
        -file_settings["world_size_cm_y"] / 2,
        file_settings["world_size_cm_x"] / 2,
        file_settings["world_size_cm_y"] / 2,
    ]

    orientations = []
    # Iterate all paths
    for poses_per_path in poses_from_paths:
        # Iterate all files
        for poses in poses_per_path:
            reshaped_poses = poses.reshape((-1, 4))

            xbins = np.linspace(world_bounds[0], world_bounds[2], 11)
            ybins = np.linspace(world_bounds[1], world_bounds[3], 11)
            ret_1 = stats.binned_statistic_2d(
                reshaped_poses[:, 0],
                reshaped_poses[:, 1],
                reshaped_poses[:, 2],
                "mean",
                bins=[xbins, ybins],
            )
            ret_2 = stats.binned_statistic_2d(
                reshaped_poses[:, 0],
                reshaped_poses[:, 1],
                reshaped_poses[:, 3],
                "mean",
                bins=[xbins, ybins],
            )

        orientations.append((ret_1, ret_2))

    fig, ax = plt.subplots(1, len(orientations), figsize=(8 * len(orientations), 8))
    if len(orientations) == 1:
        ax = [ax]

    for i in range(len(orientations)):
        orientation = orientations[i]
        s_1, x_edges, y_edges, bnr = orientation[0]
        s_2, x_edges, y_edges, bnr = orientation[1]

        ax[i].set_title("Mean orientation in tank (%s)" % labels[i])
        ax[i].set_xlabel("x [cm]")
        ax[i].set_ylabel("y [cm]")

        xx, yy = np.meshgrid(x_edges, y_edges)

        plot = ax[i].pcolormesh(
            xx,
            yy,
            np.arctan2(s_2, s_1).T,
            vmin=-np.pi,
            vmax=np.pi,
            cmap="twilight",
        )
        cbar = plt.colorbar(plot, ax=ax[i], pad=0.015, aspect=10)
        show_values(plot)

    return fig


def evaluate_relative_orientation(
    paths: Iterable[Union[str, Path]],
    labels: Iterable[str] = None,
    predicate: Callable[[robofish.io.Entity], bool] = None,
    poses_from_paths: Iterable[Iterable[np.ndarray]] = None,
    file_settings: dict = None,
) -> matplotlib.figure.Figure:
    """Evaluate the relative orientations of the entities as a histogram.

    Lambda function example: lambda e: e.category == "fish"

    Args:
        paths(Iterable[Union[str, Path]]): An array of strings, with files of folders.
        labels(Iterable[str]): Labels for the paths. If no labels are given, the paths will be used.
        predicate(Callable[[robofish.io.Entity], bool]): a lambda function, selecting entities
        poses_from_paths(Iterable[Iterable[np.ndarray]]): An array of poses for each path.
        file_settings(dict): Settings for the files.
    Returns:
        matplotlib.figure.Figure: The figure of the histogram.
    """

    if poses_from_paths is None:
        poses_from_paths, file_settings = utils.get_all_poses_from_paths(
            paths, predicate
        )

    orientations = []
    # Iterate all paths
    for poses_per_path in poses_from_paths:
        path_orientations = []

        # Iterate all files
        for poses in poses_per_path:
            for i in range(len(poses)):
                angle1 = np.arctan2(poses[i, :, 3], poses[i, :, 2])
                for j in range(len(poses)):
                    if i != j:
                        angle2 = np.arctan2(poses[j, :, 3], poses[i, :, 2])
                        path_orientations.extend(
                            utils.limit_angle_range(angle1 - angle2)
                        )

        orientations.append(path_orientations)

    fig = plt.figure()
    plt.hist(orientations, bins=40, label=labels, density=True, range=[-np.pi, np.pi])
    plt.title("Relative orientation")
    plt.xlabel("orientation in radians")
    plt.ylabel("Frequency")
    plt.ticklabel_format(useOffset=False)
    plt.legend()
    # plt.tight_layout()

    return fig


def evaluate_distance_to_wall(
    paths: Iterable[Union[str, Path]],
    labels: Iterable[str] = None,
    predicate: Callable[[robofish.io.Entity], bool] = None,
    poses_from_paths: Iterable[Iterable[np.ndarray]] = None,
    file_settings: dict = None,
) -> matplotlib.figure.Figure:
    """Evaluate the distances of the entities to the walls as a histogram.
    Lambda function example: lambda e: e.category == "fish"

    Args:
        paths(Iterable[Union[str, Path]]): An array of strings, with files of folders.
        labels(Iterable[str]): Labels for the paths. If no labels are given, the paths will be used.
        predicate(Callable[[robofish.io.Entity], bool]): a lambda function, selecting entities
        poses_from_paths(Iterable[Iterable[np.ndarray]]): An array of poses for each path.
        file_settings(dict): Settings for the files.
    Returns:
        matplotlib.figure.Figure: The figure of the histogram.
    """

    if poses_from_paths is None:
        poses_from_paths, file_settings = utils.get_all_poses_from_paths(
            paths, predicate
        )

    world_bounds = [
        -file_settings["world_size_cm_x"] / 2,
        -file_settings["world_size_cm_y"] / 2,
        file_settings["world_size_cm_x"] / 2,
        file_settings["world_size_cm_y"] / 2,
    ]
    wall_lines = [
        (world_bounds[0], world_bounds[1], world_bounds[0], world_bounds[3]),
        (world_bounds[0], world_bounds[1], world_bounds[2], world_bounds[1]),
        (world_bounds[2], world_bounds[3], world_bounds[2], world_bounds[1]),
        (world_bounds[2], world_bounds[3], world_bounds[0], world_bounds[3]),
    ]

    distances = []

    # Iterate all paths
    for poses_per_path in poses_from_paths:
        path_distances = []

        # Iterate all files
        for poses in poses_per_path:
            for e_poses in poses:
                dist = []
                for wall in wall_lines:
                    dist.append(
                        calculate_distLinePoint(
                            wall[0], wall[1], wall[2], wall[3], e_poses
                        )
                    )
                # use distance only from closest wall
                dist = np.stack(dist).min(axis=0)
                path_distances.extend(dist)

        distances.append(path_distances)

    fig = plt.figure()

    plt.hist(
        distances,
        bins=30,
        label=labels,
        density=True,
        range=[
            0,
            # Maximum distance is always half of the shorter side.
            min(
                file_settings["world_size_cm_x"] / 2,
                file_settings["world_size_cm_y"] / 2,
            ),
        ],
    )
    plt.title("Distance to closest wall")
    plt.xlabel("Distance [cm]")
    plt.ylabel("Frequency")
    plt.tight_layout()
    plt.ticklabel_format(useOffset=False)
    plt.legend()
    # plt.tight_layout()

    return fig


def evaluate_tank_position(
    paths: Iterable[Union[str, Path]],
    labels: Iterable[str] = None,
    predicate: Callable[[robofish.io.Entity], bool] = None,
    poses_from_paths: Iterable[Iterable[np.ndarray]] = None,
    file_settings: dict = None,
    max_points: int = 4000,
) -> matplotlib.figure.Figure:
    """Evaluate the positions of the entities as a heatmap.
    Lambda function example: lambda e: e.category == "fish"

    Args:
        paths(Iterable[Union[str, Path]]): An array of strings, with files of folders.
        labels(Iterable[str]): Labels for the paths. If no labels are given, the paths will be used.
        predicate(Callable[[robofish.io.Entity], bool]): a lambda function, selecting entities
        poses_from_paths(Iterable[Iterable[np.ndarray]]): An array of poses for each path.
        file_settings(dict): Settings for the files.
        max_points(int): Maximum number of points to plot.
    Returns:
        matplotlib.figure.Figure: The figure of the heatmap.
    """

    if poses_from_paths is None:
        poses_from_paths, file_settings = utils.get_all_poses_from_paths(
            paths, predicate
        )

    xy_positions = []

    # Iterate all paths

    for poses_per_path in poses_from_paths:
        # Get all positions for each file (skipping steps with poses_step), flatten them to (..., 2) and concatenate all positions.
        new_xy_positions = np.concatenate(
            [p[:, :, :2].reshape(-1, 2) for p in poses_per_path], axis=0
        )

        xy_positions.append(new_xy_positions)

    fig, ax = plt.subplots(1, len(xy_positions), figsize=(8 * len(xy_positions), 8))
    if len(xy_positions) == 1:
        ax = [ax]

    for i in range(len(xy_positions)):
        ax[i].set_xlim(
            -file_settings["world_size_cm_x"] / 2, file_settings["world_size_cm_x"] / 2
        )
        ax[i].set_ylim(
            -file_settings["world_size_cm_y"] / 2, file_settings["world_size_cm_y"] / 2
        )

        ax[i].set_xlabel("x [cm]")
        ax[i].set_ylabel("y [cm]")

        samples = min(max_points, len(xy_positions[i]))
        idx = np.random.choice(range(samples), samples, replace=False)
        reduced_xy_positions = xy_positions[i][idx]

        ax[i].set_title(
            f"Tankpositions {labels[i]}\n{reduced_xy_positions.shape[0]} samples of {len(xy_positions[i])} total"
        )
        sns.kdeplot(
            x=reduced_xy_positions[:, 0],
            y=reduced_xy_positions[:, 1],
            n_levels=20,
            fill=True,
            ax=ax[i],
        )

    return fig


def evaluate_quiver(
    paths: Iterable[Union[str, Path]],
    labels: Iterable[str] = None,
    predicate: Callable[[robofish.io.Entity], bool] = None,
    poses_from_paths: Iterable[Iterable[np.ndarray]] = None,
    speeds_turns_from_paths: Iterable[Iterable[np.ndarray]] = None,
    file_settings: dict = None,
    max_files: int = None,
    bins: int = 25,
) -> matplotlib.figure.Figure:
    """BETA: Plot the flow of movement in the files.
    Lambda function example: lambda e: e.category == "fish"

    Args:
        paths(Iterable[Union[str, Path]]): An array of strings, with files of folders.
        labels(Iterable[str]): Labels for the paths. If no labels are given, the paths will be used.
        predicate(Callable[[robofish.io.Entity], bool]): a lambda function, selecting entities
        poses_from_paths(Iterable[Iterable[np.ndarray]]): An array of poses for each path.
        speeds_turns_from_paths(Iterable[Iterable[np.ndarray]]): An array of speeds and turns for each path.
        file_settings(dict): Settings for the files.
        max_files(int): Maximum number of files to plot.
        bins(int): Number of bins for the histogram.
    Returns:
        matplotlib.figure.Figure: The figure of the quiver plot.
    """
    try:
        import torch
        from fish_models.models.pascals_lstms.attribution import SocialVectors
    except ImportError:
        print(
            "Either torch or fish_models is not installed.",
            "The social vector should come from robofish io.",
            "This is a known issue (#24)",
        )
        return

    if poses_from_paths is None:
        poses_from_paths, file_settings = utils.get_all_poses_from_paths(
            paths, predicate, predicate
        )
    if speeds_turns_from_paths is None:
        speeds_turns_from_paths, file_settings = utils.get_all_data_from_paths(
            paths, "speeds_turns", max_files=max_files
        )
    # print(poses_from_paths.shape)
    if len(poses_from_paths) != 1:
        warnings.warn(
            "NotImplemented: Only one path is supported for now in quiver plot."
        )
        poses_from_paths = poses_from_paths[:1]

    poses_from_paths = np.array(poses_from_paths)

    # if speeds_turns_from_paths.dtype == np.object:
    #    print(
    #        "This will probably fail because the type of speeds_turns_from_path is object"
    #    )
    #    print(speeds_turns_from_paths)
    try:
        print(speeds_turns_from_paths)
        speeds_turns_from_paths = np.stack(
            [np.array(st, dtype=np.float32) for st in speeds_turns_from_paths]
        )
    except Exception as e:
        warnings.warn(
            f"The conversion to numpy array failed:\nlen(speeds_turns_from_path) was {len(speeds_turns_from_paths)}. Exception was {e}"
        )
        return
    print(speeds_turns_from_paths.dtype)
    print(speeds_turns_from_paths)
    all_poses = torch.tensor(poses_from_paths)[0, :, :, :-1].reshape((-1, 4))
    speed = torch.tensor(speeds_turns_from_paths)[0, ..., 0].flatten()
    all_poses_speed = torch.clone(all_poses)
    all_poses_speed[:, 2] *= speed
    all_poses_speed[:, 3] *= speed

    tank_b = torch.linspace(
        torch.min(all_poses[:, :2]), torch.max(all_poses[:, :2]), bins
    )
    nb = len(tank_b)
    poses_buckets = torch.bucketize(all_poses[:, :2], tank_b)

    tank_directions = np.zeros((nb, nb, 2))
    tank_directions_speed = np.zeros_like(tank_directions)
    tank_count = np.zeros((nb, nb))

    for x in tqdm(range(nb)):
        for y in range(nb):
            d = torch.where(
                torch.logical_and(poses_buckets[:, 0] == x, poses_buckets[:, 1] == y)
            )[0]
            if len(d) > 0:
                # print("d", d)
                # d_v = torch.stack((torch.cos(all_poses[d,2]), torch.sin(all_poses[d,2])), dim=1)
                # print("dv",d_v.shape)
                # tank_directions[x, y] = torch.mean(all_poses[d, 2:], dim=0)
                tank_directions_speed[x, y] = torch.mean(all_poses_speed[d, 2:], dim=0)
                # print(tank_directions[x,y] - tank_directions_speed[x,y])
                tank_count[x, y] = len(d)

    sv = SocialVectors(poses_from_paths[0])
    sv_r = torch.tensor(sv.social_vectors_without_focal_zeros)[:, :, :-1].reshape(
        (-1, 3)
    )  # [:1000]
    sv_r = torch.cat(
        (sv_r[:, :2], torch.cos(sv_r[:, 2:]), torch.sin(sv_r[:, 2:])), dim=1
    )
    sv_r_s = torch.clone(sv_r)
    sv_r_s[:, 2] *= speed
    sv_r_s[:, 3] *= speed

    social_b = torch.linspace(-20, 20, bins)
    nb = len(social_b)
    poses_buckets = torch.bucketize(sv_r[:, :2], social_b)

    social_directions = np.zeros((nb, nb, 2))
    social_directions_speed = np.zeros_like(social_directions)
    social_count = np.zeros((nb, nb))

    for x in tqdm(range(nb)):
        for y in range(nb):
            d = torch.where(
                torch.logical_and(poses_buckets[:, 0] == x, poses_buckets[:, 1] == y)
            )[0]
            if len(d) > 0:
                # print("d", d)

                # print("dv",d_v.shape)
                social_directions[x, y] = torch.mean(sv_r[d, 2:], dim=0)
                social_directions_speed[x, y] = torch.mean(sv_r_s[d, 2:], dim=0)
                social_count[x, y] = len(d)

    tank_xs, tank_ys = np.meshgrid(tank_b, tank_b)
    social_xs, social_ys = np.meshgrid(social_b, social_b)
    fig, axs = plt.subplots(1, 2, figsize=(20, 10))

    axs[0].quiver(
        tank_xs,
        tank_ys,
        tank_directions_speed[..., 0],
        -tank_directions_speed[..., 1],
        alpha=tank_count / (np.max(tank_count)),
    )
    axs[0].set_title("Avg Direction weighted by speed")
    axs[1].quiver(
        social_xs,
        social_ys,
        social_directions_speed[..., 0],
        -social_directions_speed[..., 1],
        alpha=0.3 + social_count * 0.7 / (np.max(social_count)),
    )
    axs[1].set_title("Avg Direction weighted by speed in social situations")

    fish_size = 50 / 20
    linewidth = 2

    head = axs[1].add_patch(
        plt.Circle(
            xy=(0, 0),
            radius=fish_size,
            linewidth=linewidth,
            # color="white",
            edgecolor="black",
            facecolor="white",
            fill=True,
            alpha=0.8,
        )
    )

    (tail,) = axs[1].plot(
        [-fish_size, -fish_size * 2],
        [0, 0],
        color="black",
        linewidth=linewidth,
    )
    return fig


def evaluate_social_vector(
    paths: Iterable[Union[str, Path]],
    labels: Iterable[str] = None,
    predicate: Callable[[robofish.io.Entity], bool] = None,
    poses_from_paths: Iterable[Iterable[np.ndarray]] = None,
    file_settings: dict = None,
) -> matplotlib.figure.Figure:
    """Evaluate the vectors pointing from the focal fish to the conspecifics as heatmap.
    Lambda function example: lambda e: e.category == "fish"

    Args:
        paths(Iterable[Union[str, Path]]): An array of strings, with files of folders.
        labels(Iterable[str]): Labels for the paths. If no labels are given, the paths will be used.
        predicate(Callable[[robofish.io.Entity], bool]): a lambda function, selecting entities
        poses_from_paths(Iterable[Iterable[np.ndarray]]): An array of poses for each path.
        file_settings(dict): Settings for the files.
    Returns:
        matplotlib.figure.Figure: The figure of the social vectors.
    """

    try:
        from fish_models.models.pascals_lstms.attribution import SocialVectors
    except ImportError:
        warnings.warn("Please install the fish_models package to use this function.")
        return

    if poses_from_paths is None:
        poses_from_paths, file_settings = utils.get_all_poses_from_paths(
            paths, predicate
        )

    fig, ax = plt.subplots(
        1, len(poses_from_paths), figsize=(8 * len(poses_from_paths), 8)
    )
    if len(poses_from_paths) == 1:
        ax = [ax]

    # Iterate all paths
    for i, poses_per_path in enumerate(poses_from_paths):
        poses = np.stack(poses_per_path)
        poses = np.concatenate(
            [
                poses[..., :2],
                np.arctan2(poses[..., 3], poses[..., 2])[..., None],
            ],
            axis=-1,
        )

        social_vec = SocialVectors(poses).social_vectors_without_focal_zeros
        flat_sv = social_vec.reshape((-1, 3))

        bins = (
            30 if flat_sv.shape[0] < 40000 else 50 if flat_sv.shape[0] < 65000 else 100
        )

        ax[i].hist2d(
            flat_sv[:, 0], flat_sv[:, 1], range=[[-7.5, 7.5], [-7.5, 7.5]], bins=bins
        )
        ax[i].set_title(labels[i])
        if bins < 100:
            ax[i].set_title(
                f"{labels[i]} downsampled to {bins} bins.\nFor more details generate {60000/flat_sv.shape[0]:.1f} times as much."
            )
    plt.suptitle("Social Vectors")
    return fig


def evaluate_follow_iid(
    paths: Iterable[Union[str, Path]],
    labels: Iterable[str] = None,
    predicate: Callable[[robofish.io.Entity], bool] = None,
    poses_from_paths: Iterable[Iterable[np.ndarray]] = None,
    file_settings: dict = None,
) -> matplotlib.figure.Figure:
    """Evaluate the follow metric in respect to the inter individual distance (iid).
    Lambda function example: lambda e: e.category == "fish"

    Args:
        paths(Iterable[Union[str, Path]]): An array of strings, with files of folders.
        labels(Iterable[str]): Labels for the paths. If no labels are given, the paths will be used.
        predicate(Callable[[robofish.io.Entity], bool]): a lambda function, selecting entities
        poses_from_paths(Iterable[Iterable[np.ndarray]]): An array of poses for each path.
        file_settings(dict): Settings for the files.
    Returns:
        matplotlib.figure.Figure: The figure of the follow iids.
    """

    if poses_from_paths is None:
        poses_from_paths, file_settings = utils.get_all_poses_from_paths(
            paths, predicate
        )

    follow, iid = [], []

    # Iterate all paths
    for poses_per_path in poses_from_paths:
        path_iid, path_follow = [], []

        # Iterate all files
        for poses in poses_per_path:
            for i in range(len(poses)):
                for j in range(len(poses)):
                    if i != j and (poses[i] != poses[j]).any():
                        path_iid.append(
                            calculate_iid(poses[i, :-1, :2], poses[j, :-1, :2])
                        )

                        file_follow = calculate_follow(poses[i, :, :2], poses[j, :, :2])

                        # Remove inf values and replace them with 0
                        file_follow = np.where(np.isnan(file_follow), 0, file_follow)
                        file_follow = np.where(np.isinf(file_follow), 0, file_follow)

                        path_follow.append(file_follow)

        follow.append(np.concatenate([np.atleast_1d(f) for f in path_follow]))
        iid.append(np.concatenate([np.atleast_1d(i) for i in path_iid]))

    grids = []

    # Find the 0.5%/ 99.5% quantiles as min and max for the axis
    follow_range = np.max(
        [
            -1 * np.quantile(np.concatenate(follow), 0.1),
            np.quantile(np.concatenate(follow), 0.9),
        ]
    )
    max_iid = np.quantile(np.concatenate(iid), 0.9)

    for i in range(len(follow)):
        # Mask data which is outside of the ranges
        mask = (
            (follow[i] > -1 * follow_range)
            & (follow[i] < follow_range)
            & (iid[i] < max_iid)
        )

        follow_iid_data = pd.DataFrame(
            {
                "IID [cm]": iid[i][mask],
                "Follow": follow[i][mask],
            },
            dtype=np.float32,
        )

        plt.rcParams["lines.markersize"] = 1
        grid = sns.jointplot(
            x="IID [cm]",
            y="Follow",
            data=follow_iid_data,
            kind="hist",
            xlim=(0, max_iid),
            ylim=(-follow_range, follow_range),
            # cbar=True,
            # legend=True,
            joint_kws={"bins": 30},
            marginal_kws=dict(bins=30),
        )
        # grid.fig.set_figwidth(9)
        # grid.fig.set_figheight(6)
        # grid.fig.subplots_adjust(top=0.9)
        grids.append(grid)

    # This is neccessary because joint plot does not receive an ax object.
    # It creates issues with too many plots though
    # Created an issue
    fig = plt.figure(figsize=(6 * len(grids), 6))

    fig.suptitle(
        f"follow/iid: from left to right:\n{', '.join([str(l) for l in labels])}",
        fontsize=12,
    )
    gs = gridspec.GridSpec(1, len(grids))

    for i in range(len(grids)):
        SeabornFig2Grid(grids[i], fig, gs[i])

    return fig


def evaluate_tracks_distance(
    paths: Iterable[Union[str, Path]],
    labels: Iterable[str] = None,
    predicate: Callable[[robofish.io.Entity], bool] = None,
    poses_from_paths: Iterable[Iterable[np.ndarray]] = None,
    file_settings: dict = None,
    max_timesteps: int = 4000,
) -> matplotlib.figure.Figure:
    """Evaluate the distances of two or more fish on the track.
    Lambda function example: lambda e: e.category == "fish"

    Args:
        paths(Iterable[Union[str, Path]]): An array of strings, with files of folders.
        labels(Iterable[str]): Labels for the paths. If no labels are given, the paths will be used.
        predicate(Callable[[robofish.io.Entity], bool]): a lambda function, selecting entities
        poses_from_paths(Iterable[Iterable[np.ndarray]]): An array of poses for each path.
        file_settings(dict): Settings for the files.
        max_timesteps(int): The maximum number of timesteps to plot.
    Returns:
        matplotlib.figure.Figure: The figure of the tracks.
    """
    return evaluate_tracks(
        paths,
        labels,
        predicate,
        lw_distances=True,
        max_timesteps=max_timesteps,
    )


def evaluate_tracks(
    paths: Iterable[Union[str, Path]],
    labels: Iterable[str] = None,
    predicate: Callable[[robofish.io.Entity], bool] = None,
    poses_from_paths: Iterable[Iterable[np.ndarray]] = None,
    file_settings: dict = None,
    lw_distances: bool = False,
    seed: int = 42,
    max_timesteps: int = None,
    verbose: bool = False,
) -> matplotlib.figure.Figure:
    """Evaluate the distances of two or more fish on the track.
    Lambda function example: lambda e: e.category == "fish"

    Args:
        paths(Iterable[Union[str, Path]]): An array of strings, with files of folders.
        labels(Iterable[str]): Labels for the paths. If no labels are given, the paths will be used.
        predicate(Callable[[robofish.io.Entity], bool]): a lambda function, selecting entities
        poses_from_paths(Iterable[Iterable[np.ndarray]]): An array of poses for each path.
        file_settings(dict): Settings for the files.
        lw_distances(bool): If true, the distances will be represented by the thickness of the line.
        seed(int): The seed for the random number generator.
        max_timesteps(int): The maximum number of timesteps to plot.
        verbose(bool): If true, the progress will be printed.
    Returns:
        matplotlib.figure.Figure: The figure of the tracks.
    """

    paths = [Path(p) for p in paths]
    random.seed(seed)

    files_per_path = utils.get_all_files_from_paths(paths)

    max_files_per_path = max([len(files) for files in files_per_path])
    rows, cols = len(files_per_path), min(6, max_files_per_path)

    multirow = False
    if rows == 1 and cols == 4:
        rows = min(3, int(np.ceil(max_files_per_path / 4)))
        multirow = True

    fig, ax = plt.subplots(rows, cols, figsize=(cols * 4, rows * 4), squeeze=False)

    initial_poses_info_available = None
    all_initial_info = []

    # Iterate all paths
    for k, files_in_path in enumerate(files_per_path):
        random.shuffle(files_in_path)

        # Iterate all files
        for i, file_path in enumerate(files_in_path):
            if multirow:
                if i >= cols * rows:
                    break
                selected_ax = ax[i // cols][i % cols]
            else:
                if i >= cols:
                    break
                selected_ax = ax[k][i]

            if (
                initial_poses_info_available is not None
                and initial_poses_info_available < k
            ):
                if len(all_initial_info) > i:
                    initial_poses_info = all_initial_info[i]

                    preload_data = Path(initial_poses_info["preload_data"])

                    assert (
                        preload_data.parts == paths[k].parts[-len(preload_data.parts) :]
                    ), f"The second given path should correspond to the preload_data of the second path.\nExpected preload: {preload_data.parts}\nGiven Path: {paths[k].parts[-len(preload_data.parts) :]}"

                    # Open the correct file instead.

                    new_file_path = paths[k] / initial_poses_info["file_name"]
                    if verbose:
                        print(f"Using file {file_path}")
                    with robofish.io.File(new_file_path, "r") as new_file:
                        new_file.plot(
                            selected_ax,
                            lw_distances=lw_distances,
                            skip_timesteps=initial_poses_info["start"],
                            max_timesteps=initial_poses_info["n_timesteps"],
                        )
                        selected_ax.set_title(
                            f"{selected_ax.get_title()} (reference track: id {initial_poses_info['track_id']})",
                        )
            else:
                with robofish.io.File(file_path, "r") as file:
                    if (
                        "initial_poses_info" in file
                        and "track_id" in file["initial_poses_info"].attrs
                    ):
                        # Save the corresponding file that a model mimics.
                        initial_poses_info = {
                            a: file["initial_poses_info"].attrs[a]
                            for a in file["initial_poses_info"].attrs.keys()
                        }

                        initial_poses_info["n_timesteps"] = file.entity_poses.shape[1]

                        all_initial_info.append(initial_poses_info)
                        initial_poses_info_available = k

                        file.plot(
                            selected_ax,
                            lw_distances=lw_distances,
                            max_timesteps=max_timesteps,
                        )
                        selected_ax.set_title(
                            f"{selected_ax.get_title()} (from track_id {initial_poses_info['track_id']})",
                        )
                    else:
                        file.plot(
                            selected_ax,
                            lw_distances=lw_distances,
                            max_timesteps=max_timesteps,
                        )

    plt.tight_layout()

    return fig


def evaluate_individual_speed(
    speeds_turns_from_paths: Iterable[Iterable[np.ndarray]] = None,
    file_settings: dict = None,
    max_files: int = None,
    **kwargs: dict,
) -> matplotlib.figure.Figure:
    """Evaluate the average speeds per individual.

    The usual poltting arguments can be added and are passed to the plot function.

    Args:
        speeds_turns_from_paths(Iterable[Iterable[np.ndarray]]): An array of speeds and turns for each path.
        file_settings(dict): Settings for the files.
        max_files(int): The maximum number of files to plot.
        kwargs(dict): Additional arguments for the plot function.
    Returns:
        matplotlib.figure.Figure: The figure of the individual speeds.
    """
    return evaluate_individuals(
        speeds_turns_from_paths=speeds_turns_from_paths,
        file_settings=file_settings,
        mode="speed",
        max_files=max_files,
        **kwargs,
    )


def evaluate_individual_iid(
    poses_from_paths: Iterable[Iterable[np.ndarray]] = None,
    file_settings: dict = None,
    max_files: int = None,
    **kwargs: dict,
) -> matplotlib.figure.Figure:
    """Evaluate the average iid per file

    The usual poltting arguments can be added and are passed to the plot function.

    Args:
        poses_from_paths(Iterable[Iterable[np.ndarray]]): An array of poses for each path.
        file_settings(dict): Settings for the files.
        max_files(int): The maximum number of files to plot.
        kwargs(dict): Additional arguments for the plot function.
    Returns:
        matplotlib.figure.Figure: The figure of the individual iids.
    """
    return evaluate_individuals(
        poses_from_paths=poses_from_paths,
        file_settings=file_settings,
        mode="iid",
        max_files=max_files,
        **kwargs,
    )


def evaluate_individuals(
    mode: str,
    paths: Iterable[Union[str, Path]],
    labels: Iterable[str] = None,
    speeds_turns_from_paths: Iterable[Iterable[np.ndarray]] = None,
    poses_from_paths: Iterable[Iterable[np.ndarray]] = None,
    file_settings: dict = None,
    predicate: Callable[[robofish.io.Entity], bool] = None,
    threshold: int = 7,
    max_files: int = None,
) -> matplotlib.figure.Figure:
    """Evaluate the individuals. Mode specifies if speed or iid is evaluated.
    predicate example: lambda e: e.category == "fish"

    Args:
        mode(str): A choice between ['speed', 'iid']
        paths(Iterable[Union[str, Path]]): The paths to the files.
        labels(Iterable[str]): The labels of the paths. If no labels are given, the paths will be used
        speeds_turns_from_paths(Iterable[Iterable[np.ndarray]]): An array of speeds and turns for each path.
        poses_from_paths(Iterable[Iterable[np.ndarray]]): An array of poses for each path.
        file_settings(dict): Settings for the files.
        predicate(Callable[[robofish.io.Entity], bool]): A lambda function, selecting entitiespredicate to filter the files.
        threshold(int): The threshold for the metric, the threshold is shown in the plot and files below are printed.
        max_files(int): The maximum number of files to plot.
    Returns:
        matplotlib.figure.Figure: The figure of the individual metrics.
    """

    if speeds_turns_from_paths is None and mode == "speed":
        speeds_turns_from_paths, file_settings = utils.get_all_data_from_paths(
            paths, "speeds_turns", max_files=max_files, predicate=predicate
        )
    if poses_from_paths is None and mode == "iid":
        poses_from_paths, file_settings = utils.get_all_data_from_paths(
            paths, "poses_4d", max_files=max_files, predicate=predicate
        )

    files_from_paths = utils.get_all_files_from_paths(paths, max_files)

    fig = plt.figure(figsize=(10, 4))
    # small_iid_files = []
    offset = 0
    for k, files_in_paths in enumerate(files_from_paths):
        all_avg = []
        all_std = []

        for f, file_path in enumerate(files_in_paths):
            if mode == "speed":
                metric = (
                    speeds_turns_from_paths[k][f][..., 0]
                    * file_settings["frequency_hz"]
                )
            elif mode == "iid":
                poses = poses_from_paths[k][f]
                if poses.shape[0] != 2:
                    print(
                        "The evaluate_individual_iid function only works when there are exactly 2 individuals. If you need it for multiple agents please implement it."
                    )
                    return
                metric = calculate_iid(poses[0], poses[1])[None]
                # mean = np.mean(metric, axis=1)[0]
                # if mean < 7:
                #    small_iid_files.append(str(file.path))

            all_avg.append(np.nanmean(metric, axis=1))
            all_std.append(np.nanstd(metric, axis=1))

        all_avg = np.concatenate(all_avg, axis=0)
        all_std = np.concatenate(all_std, axis=0)
        individuals = all_avg.shape[0]

        plt.errorbar(
            np.arange(offset, individuals + offset),
            all_avg,
            all_std,
            label=labels[k],
            fmt="o",
        )
        offset += individuals

    if mode == "iid":
        plt.plot([0, offset], [threshold, threshold], c="red", alpha=0.5)

    text = {
        "speed": {
            "title": "Average speed per individual",
            "ylabel": "Average Speed (cm / s) +/- Std Dev",
            "xlabel": "Individual ID",
        },
        "iid": {
            "title": "Average iid per individual",
            "ylabel": "Average iid (cm) +/- Std Dev",
            "xlabel": "File ID",
        },
    }
    this_text = text[mode]
    plt.title(this_text["title"])
    plt.legend(loc="upper right")
    plt.xlabel(this_text["xlabel"])
    plt.ylabel(this_text["ylabel"])
    plt.tight_layout()

    # This can be used to return a test data and training data split
    # np.random.shuffle(small_iid_files)
    # train_part = int(len(small_iid_files) * 0.8)

    # print("TRAINING DATA:")
    # print(" ".join(small_iid_files[:train_part]))

    # print("TEST DATA:")
    # print(" ".join(small_iid_files[train_part:]))
    return fig


def evaluate_all(
    paths: Iterable[Union[str, Path]],
    labels: Iterable[str] = None,
    save_folder: Path = None,
    fdict: dict = None,
    predicate: Callable[[robofish.io.Entity], bool] = None,
    max_files: int = None,
    evaluations: List[str] = None,
    file_format: str = "png",
) -> Iterable[Path]:
    """Generate all evaluation graphs and save them to a folder.

    Args:
        paths(Iterable[Union[str, Path]]): The paths to the files.
        labels(Iterable[str]): The labels of the paths. If no labels are given, the paths will be used
        save_folder(Path): The folder to save the graphs to.
        fdict(dict): The dictionary with the functions which should be called. If none is given, all functions are used.
        predicate(Callable[[robofish.io.Entity], bool]): A lambda function, selecting entities.
        max_files(int): The maximum number of files to plot.
        evaluations(List[str]): A list of all evaluations to plot. Values should correspond to the keys in fdict.
    Returns:
        Iterable[Path]: An array of all paths to the created images.
    """
    assert (
        save_folder is not None and save_folder != ""
    ), "Please provide a save_folder using --save_path"

    save_folder.mkdir(exist_ok=True, parents=True)

    save_paths = []
    if fdict is None:
        fdict = robofish.evaluate.app.function_dict()
        fdict.pop("all")

    poses_from_paths, file_settings = utils.get_all_poses_from_paths(
        paths, max_files=max_files
    )
    speeds_turns_from_paths, file_settings = utils.get_all_data_from_paths(
        paths, "speeds_turns", max_files=max_files
    )

    input_dict = {
        "poses_from_paths": poses_from_paths,
        "speeds_turns_from_paths": speeds_turns_from_paths,
        "file_settings": file_settings,
        "max_files": max_files,
        "seed": np.random.randint(0, 100),
    }

    t = tqdm(fdict.items(), desc="Evaluation", leave=True)
    for f_name, f_callable in t:
        if evaluations is None or f_name in evaluations:
            t.set_description(f_name)
            t.refresh()  # to show the update immediately
            save_path = save_folder / (f_name + "." + file_format)

            requested_inputs = {
                k: input_dict[k]
                for k in inspect.signature(f_callable).parameters.keys()
                if k in input_dict
            }
            fig = f_callable(
                paths=paths, labels=labels, predicate=predicate, **requested_inputs
            )
            if fig is not None:
                # Increase resolution
                fig.savefig(save_path, dpi=300, bbox_inches="tight")
                plt.close(fig)
                save_paths.append(save_path)

    return save_paths


def calculate_follow(a: np.ndarray, b: np.ndarray) -> np.ndarray:
    """Calculate the follow metric.

    Given two series of poses - with X and Y coordinates of their
    positions as the first two elements
    return the follow metric from the first to the second series.
    Args:
        a(np.ndarray): The first series of poses.
        b(np.ndarray): The second series of poses.
    Returns:
        np.ndarray: The follow metric.
    """
    a_v = a[1:, :2] - a[:-1, :2]
    b_p = normalize_series(b[:-1, :2] - a[:-1, :2])
    return (a_v * b_p).sum(axis=-1)


def calculate_iid(a: np.ndarray, b: np.ndarray) -> np.ndarray:
    """Calculate the iid metric.

    Given two series of poses - with X and Y coordinates of their positions
    as the first two elements
    return the inter-individual distance (between the positions).
    Args:
        a(np.ndarray): The first series of poses.
        b(np.ndarray): The second series of poses.
    Returns:
        np.ndarray: The iid metric.
    """
    return np.linalg.norm(b[:, :2] - a[:, :2], axis=-1)


def calc_tlvc(
    a: np.ndarray, b: np.ndarray, tau_min: float, tau_max: float
) -> np.ndarray:
    """
    Given two velocity series and both minimum and maximum time lag return the
    time lagged velocity correlation from the first to the second series.

    Args:
        a(np.ndarray): The first series of poses.
        b(np.ndarray): The second series of poses.
        tau_min(float): The minimum time lag.
        tau_max(float): The maximum time lag.
    Returns:
        np.ndarray: The iid metric.
    """
    length = tau_max - tau_min
    return np.float32(
        [
            (a[t] @ b[t + tau_min :][:length].T).mean()
            for t in range(min(len(a), len(b) - tau_max + 1))
        ]
    )


def normalize_series(x: np.ndarray) -> np.ndarray:
    """Normalize series.

    Given a series of vectors, return a series of normalized vectors.
    Null vectors are mapped to `NaN` vectors.
    Args:
        x(np.ndarray): The series of vectors.
    Returns:
        np.ndarray: The normalized series.
    """
    return (x.T / np.linalg.norm(x, axis=-1)).T


def calculate_distLinePoint(
    x1: float, y1: float, x2: float, y2: float, points: np.ndarray
) -> np.ndarray:
    """Compute the distance between a line and points. This is used for the wall distance metric.

    Compute the distance between line (x1, y1, x2, y2) and points
    (np array of shape (n, 2), (x,y) on each row)
    returns np array of shape (n, ) with corresponding distances
    from: https://stackoverflow.com/questions/849211/
    shortest-distance-between-a-point-and-a-line-segment
    theory: http://paulbourke.net/geometry/pointlineplane/

    Args:
        x1(float): The x coordinate of the first point of the line.
        y1(float): The y coordinate of the first point of the line.
        x2(float): The x coordinate of the second point of the line.
        y2(float): The y coordinate of the second point of the line.
        points(np.ndarray): The points to compute the distance to the line.
    Returns:
        np.ndarray: The distances from the line to the points.

    """
    px = x2 - x1
    py = y2 - y1

    norm = px * px + py * py

    u = ((points[:, 0] - x1) * px + (points[:, 1] - y1) * py) / float(norm)

    u = np.where(u > 1, 1, u)
    u = np.where(u < 0, 0, u)

    x = x1 + u * px
    y = y1 + u * py

    dx = x - points[:, 0]
    dy = y - points[:, 1]

    dist = (dx * dx + dy * dy) ** 0.5

    return dist


def show_values(
    pc: matplotlib.collections.PolyCollection, fmt: str = "%.2f", **kw: dict
) -> None:
    """Show numbers on plt.ax.pccolormesh plot.

    https://stackoverflow.com/questions/25071968/
    heatmap-with-text-in-each-cell-with-matplotlibs-pyplot

    Args:
        pc(matplotlib.collections.PolyCollection): The plot to show the values on.
        fmt(str): The format of the values.
        kw(dict): The keyword arguments.
    """
    pc.update_scalarmappable()
    ax = pc.axes
    for p, color, value in zip(pc.get_paths(), pc.get_facecolors(), pc.get_array()):
        x, y = p.vertices[:-2, :].mean(0)
        if np.all(color[:3] > 0.5):
            color = (0.0, 0.0, 0.0)
        else:
            color = (1.0, 1.0, 1.0)
        if value == "--":
            ax.text(x, y, fmt % value, ha="center", va="center", color=color, **kw)


class SeabornFig2Grid:
    """Seaborn Figure class.

    copied from https://stackoverflow.com/questions/35042255/
    how-to-plot-multiple-seaborn-jointplot-in-subplot
    """

    def __init__(
        self,
        seaborngrid: sns.axisgrid.Grid,
        fig: matplotlib.figure.Figure,
        subplot_spec: matplotlib.gridspec.SubplotSpec,
    ) -> None:
        """Init function.

        Args:
            seaborngrid(sns.axisgrid.Grid): The seaborn grid.
            fig(matplotlib.figure.Figure): The figure.
            subplot_spec(matplotlib.gridspec.SubplotSpec): The subplot spec.
        """
        self.fig = fig
        self.sg = seaborngrid
        self.subplot = subplot_spec
        if isinstance(self.sg, sns.axisgrid.FacetGrid) or isinstance(
            self.sg, sns.axisgrid.PairGrid
        ):
            self._movegrid()
        elif isinstance(self.sg, sns.axisgrid.JointGrid):
            self._movejointgrid()
        self._finalize()

    def _movegrid(self) -> None:
        """Move PairGrid or Facetgrid."""
        self._resize()
        n = self.sg.axes.shape[0]
        m = self.sg.axes.shape[1]
        self.subgrid = gridspec.GridSpecFromSubplotSpec(n, m, subplot_spec=self.subplot)
        for i in range(n):
            for j in range(m):
                self._moveaxes(self.sg.axes[i, j], self.subgrid[i, j])

    def _movejointgrid(self) -> None:
        """Move Jointgrid."""
        h = self.sg.ax_joint.get_position().height
        h2 = self.sg.ax_marg_x.get_position().height
        r = int(np.round(h / h2))
        self._resize()
        self.subgrid = gridspec.GridSpecFromSubplotSpec(
            r + 1, r + 1, subplot_spec=self.subplot
        )

        self._moveaxes(self.sg.ax_joint, self.subgrid[1:, :-1])
        self._moveaxes(self.sg.ax_marg_x, self.subgrid[0, :-1])
        self._moveaxes(self.sg.ax_marg_y, self.subgrid[1:, -1])

    def _moveaxes(self, ax: plt.Axes, gs: matplotlib.gridspec.GridSpec) -> None:
        """
        Move axes.

        https://stackoverflow.com/a/46906599/4124317

        Args:
            ax(plt.Axes): The axes to move.
            gs(matplotlib.gridspec.GridSpec): The gridspec to move the axes to.
        """
        ax.remove()
        ax.figure = self.fig
        self.fig.axes.append(ax)
        self.fig.add_axes(ax)
        ax._subplotspec = gs
        ax.set_position(gs.get_position(self.fig))
        ax.set_subplotspec(gs)

    def _finalize(self) -> None:
        """Finalize the graphics."""
        plt.close(self.sg.fig)
        self.fig.canvas.mpl_connect("resize_event", self._resize)
        self.fig.canvas.draw()

    def _resize(self) -> None:
        """Resize the graphics."""
        self.sg.fig.set_size_inches(self.fig.get_size_inches())
