import sys
import logging

import robofish.io

from robofish.evaluate.evaluate import *
import robofish.evaluate.app

if not ((3, 7) <= sys.version_info < (4, 0)):
    logging.warning("Unsupported Python version")
