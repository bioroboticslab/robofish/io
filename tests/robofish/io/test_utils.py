from robofish.io import utils

from robofish.io import utils
import numpy as np


def test_get_all_poses_from_paths():
    valid_file_path = utils.full_path(__file__, "../../resources/valid_1.hdf5")
    poses, file_settings = utils.get_all_poses_from_paths([valid_file_path])

    # (1 input array, 1 file, 2 fishes, 100 timesteps, 4 poses)
    assert np.array(poses).shape == (1, 1, 2, 100, 4)
    type(file_settings) == dict
