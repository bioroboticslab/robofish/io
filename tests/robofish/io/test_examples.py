import robofish.io
from robofish.io import utils
from pathlib import Path

import sys


sys.path.append(str(utils.full_path(__file__, "../../../examples/")))

ipynb_path = utils.full_path(__file__, "../../../examples/example_basic.ipynb")
path = utils.full_path(__file__, "../../../examples/tmp_example.hdf5")
if path.exists():
    path.unlink()


def test_example_readme():
    import example_readme

    example_readme.create_example_file(path)
    path.unlink()


def test_example_basic():
    import example_basic

    example_basic.create_example_file(path)
    path.unlink()


# This test can be executed manually. The CI/CD System has issues with testbook.
def manual_test_example_basic_ipynb():
    from testbook import testbook

    # Executing the notebook should not lead to an exception
    with testbook(str(ipynb_path), execute=True) as tb:
        pass
        # tb.ref("create_example_file")(path)
    # path.unlink()


if __name__ == "__main__":
    print("example_readme.py")
    test_example_readme()
    print("example_basic.py")
    test_example_basic()
    print("example_basic.ipynb")
    manual_test_example_basic_ipynb()
