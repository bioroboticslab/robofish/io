import robofish.io
import h5py
import numpy as np


def test_entity_object():
    sf = robofish.io.File(
        world_size_cm=[100, 100], frequency_hz=25, world_shape="rectangle"
    )
    f = sf.create_entity("fish", positions=[[10, 10]])
    assert type(f) == robofish.io.Entity, "Type of entity was wrong"
    assert f.name == "fish_1", "Name of entity was wrong"
    assert f.attrs["category"] == "fish", "category was wrong"
    print(dir(f))
    print(f["positions"])
    assert type(f["positions"]) == h5py.Dataset
    f.create_dataset("outlines", shape=(10, 10))

    timesteps = 50
    poses_rad = np.zeros((timesteps, 3))
    poses_rad[:, 2] = np.arange(0, 2 * np.pi, step=2 * np.pi / timesteps)

    f2 = sf.create_entity("fish", poses=poses_rad)
    assert type(f2["positions"]) == h5py.Dataset
    assert type(f2["orientations"]) == h5py.Dataset
    poses_rad_retrieved = f2.poses_rad

    # Check if retrieved rad poses is close to the original poses.
    # Internally always ori_x and ori_y are used. When retrieved, the range is from -pi to pi, so for some of our original data 2 pi has to be substracted.
    for i in range(timesteps):
        assert np.isclose(poses_rad[i, 2], poses_rad_retrieved[i, 2]) or np.isclose(
            poses_rad[i, 2] - 2 * np.pi, poses_rad_retrieved[i, 2]
        )


def test_entity_turns_speeds():
    """This test was created, when the turns and speeds where calculated from positions only.
    There were issues with the calculation of speeds and turns. The corresponding functions were set to be deprecated.
    This test was adapted to have plausible orientations which are used when the speeds and turns are calculated.
    The reconstructed track is not identical to the original track. The reson for this is unknown.

    There is an open issue, to check this test again: https://git.imp.fu-berlin.de/bioroboticslab/robofish/io/-/issues/14
    """

    f = robofish.io.File(
        world_size_cm=[100, 100], frequency_hz=25, world_shape="rectangle"
    )
    circle_rad = np.linspace(0, 2 * np.pi, num=100)
    circle_size = 40
    poses_rad = np.stack(
        [
            np.cos(circle_rad) * circle_size,
            np.sin(circle_rad) * circle_size,
            circle_rad + np.pi / 2,
        ],
        axis=-1,
    )

    e = f.create_entity("fish", poses=poses_rad)
    speeds_turns = e.actions_speeds_turns
    assert speeds_turns.shape == (99, 2)

    # Turns and speeds shoud be all the same afterwards, since the fish swims with constant velocity and angular velocity.
    print(np.std(speeds_turns, axis=0))
    assert (np.std(speeds_turns, axis=0) < 0.0001).all()

    # Use turn_speed to generate positions
    gen_positions = np.zeros((poses_rad.shape[0], 3))
    gen_positions[0] = e.poses_rad[0]

    for i, (speed, turn) in enumerate(speeds_turns):
        new_angle = gen_positions[i, 2] + turn
        gen_positions[i + 1] = [
            gen_positions[i, 0] + np.cos(new_angle) * speed,
            gen_positions[i, 1] + np.sin(new_angle) * speed,
            new_angle,
        ]

    # import matplotlib.pyplot as plt

    # plt.plot(poses_rad[:, 0], poses_rad[:, 1], label="real")
    # plt.plot(gen_positions[:, 0], gen_positions[:, 1], label="gen")
    # plt.legend()
    # plt.show()
    # The resulting positions should almost be equal to the the given positions
    print(gen_positions - poses_rad)
    assert np.isclose(poses_rad, gen_positions, atol=2.6).all()
