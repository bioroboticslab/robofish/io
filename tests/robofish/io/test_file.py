import robofish.io
from robofish.io import utils
import numpy as np
from pathlib import Path
import pytest
import inspect
import datetime
import sys
import logging

LOGGER = logging.getLogger(__name__)

valid_file_path = utils.full_path(__file__, "../../resources/valid_1.hdf5")
nan_file_path = utils.full_path(__file__, "../../resources/nan_test.hdf5")


def test_constructor():
    sf = robofish.io.File(world_size_cm=[100, 100], world_shape="rectangle")
    sf.validate()
    sf.close()


def test_context():
    with robofish.io.File(world_size_cm=[10, 10], world_shape="rectangle") as f:
        pass


def test_new_file_w_path(tmp_path):
    f = tmp_path / "file.hdf5"
    sf = robofish.io.File(
        f, "w", world_size_cm=[100, 100], frequency_hz=25, world_shape="rectangle"
    )
    sf.create_entity("fish")
    sf.validate()
    sf.close()


def test_missing_attribute():
    sf = robofish.io.File(world_size_cm=[10, 10], world_shape="rectangle")
    sf.attrs.pop("world_size_cm")
    assert not sf.validate(strict_validate=False)[0]
    sf.close()


def test_single_entity_frequency_hz():
    sf = robofish.io.File(
        world_size_cm=[100, 100], frequency_hz=(1000 / 40), world_shape="rectangle"
    )
    test_poses = np.ones(shape=(10, 4))
    test_poses[:, 3] = 0  # All Fish pointing right
    sf.create_entity("robofish", poses=test_poses)
    sf.create_entity("object_without_poses")
    print(sf)
    sf.validate()
    sf.close()


def test_single_entity_monotonic_time_points_us():
    with pytest.warns(Warning):
        sf = robofish.io.File(
            world_size_cm=[100, 100],
            monotonic_time_points_us=np.ones(10),
            world_shape="rectangle",
        )
    test_poses = np.ones(shape=(10, 4))
    test_poses[:, 3] = 0  # All Fish pointing right
    sf.create_entity("robofish", poses=test_poses)
    print(sf)
    sf.validate()
    sf.close()


def test_multiple_entities():
    agents = 3
    timesteps = 10

    poses = np.zeros((agents, timesteps, 4))
    poses[1] = 1
    poses[:, :, 2:] = [1, 0]  # All agents point to the right

    m_points = np.arange(timesteps)

    with pytest.warns(Warning):
        sf = robofish.io.File(
            world_size_cm=[100, 100],
            monotonic_time_points_us=m_points,
            world_shape="rectangle",
        )
    returned_entities = sf.create_multiple_entities("fish", poses)
    returned_names = [entity.name for entity in returned_entities]

    expected_names = ["fish_1", "fish_2", "fish_3"]
    print(returned_names)
    assert returned_names == expected_names
    print(sf)

    sf.validate()

    # The returned poses should be equal to the inserted poses
    returned_poses = sf.entity_poses
    print(returned_poses)
    assert (returned_poses == poses).all()

    # Just get the array for some names
    returned_poses = sf.select_entity_property(lambda e: e.name in ["fish_1", "fish_2"])
    assert (returned_poses == poses[:2]).all()

    # Filter on both category and name
    returned_poses = sf.select_entity_property(
        lambda e: e.category == "fish" and e.name == "fish_1"
    )
    assert (returned_poses == poses[:1]).all()

    # Insert some random obstacles
    obstacles = 3
    obs_poses = np.random.random((agents, 1, 3))
    returned_names = sf.create_multiple_entities("obstacle", poses=obs_poses)
    # Obstacles should not be returned when only fish are selected
    returned_poses = sf.select_entity_property(lambda e: e.category == "fish")
    assert (returned_poses == poses).all()

    # for each of the entities
    outlines = np.ones((agents, timesteps, 20, 2))

    # create new sampling
    m_points = np.ones((timesteps))
    c_points = np.empty((timesteps), dtype="O")
    c_points[:5] = "2020-12-02T10:21:58.100000+00:00"
    c_points[5:] = robofish.io.now_iso8061()

    with pytest.warns(Warning):
        new_sampling = sf.create_sampling(
            monotonic_time_points_us=m_points, calendar_time_points=c_points
        )

    returned_names = sf.create_multiple_entities(
        "fish", poses, outlines=outlines, sampling=new_sampling
    )
    print(returned_names)
    print(sf)

    # pass an poses array in separate parts (positions, orientations) and retrieve it with poses.
    poses_arr = np.random.random((100, 4))
    poses_arr[:, 2:] /= np.atleast_2d(
        np.linalg.norm(poses_arr[:, 2:], axis=1)
    ).T  # Normalization
    position_orientation_fish = sf.create_entity(
        "fish", positions=poses_arr[:, :2], orientations=poses_arr[:, 2:]
    )
    assert np.isclose(poses_arr, position_orientation_fish.poses).all()
    sf.validate()

    return sf


def test_actions_speeds_turns_angles():
    with robofish.io.File(
        world_size_cm=[100, 100], frequency_hz=25, world_shape="rectangle"
    ) as f:
        poses = np.zeros((10, 100, 3))
        f.create_multiple_entities("fish", poses=poses)

        # Stationary fish has no speed or turn
        assert (f.entity_actions_speeds_turns == 0).all()


def test_entity_poses_rad(caplog):
    with robofish.io.File(
        world_size_cm=[100, 100], frequency_hz=25, world_shape="rectangle"
    ) as f:
        # Create an entity, using radians
        f.create_entity("fish", poses=np.ones((100, 3)))

        # Read the poses of the file as radians
        np.testing.assert_almost_equal(f.entity_poses_rad, np.ones((1, 100, 3)))

        caplog.set_level(logging.WARNING)
        # Passing orientations, which are bigger than 2 pi
        f.create_entity("fish", poses=np.ones((100, 3)) * 50)
        assert "WARNING" in caplog.text


def test_entity_positions_no_orientation():
    with robofish.io.File(
        world_size_cm=[100, 100], frequency_hz=25, world_shape="rectangle"
    ) as f:
        # Create an entity, using radians
        f.create_entity("fish", positions=np.ones((100, 2)))

        # In poses, the default orientation pointing up should be added.
        assert f.entity_poses.shape == (1, 100, 4)
        assert (f.entity_poses[:, :] == np.array([1, 1, 0, 1])).all()
        assert np.isclose(f.entity_orientations_rad, np.pi / 2).all()

        # The entity is not moving so there should be no actions at all.
        assert np.isclose(f.entity_actions_speeds_turns, 0).all()


def test_load_validate():
    sf = robofish.io.File(path=valid_file_path)
    sf.validate()
    sf.close()


def test_get_entity_names():
    sf = robofish.io.File(path=valid_file_path)
    names = sf.entity_names
    assert len(names) == 2
    assert names == ["fish_1", "robot"]
    sf.close()


def test_load_copy():
    # Open a copy of a file and change an attribute
    sf = robofish.io.File(path=valid_file_path, open_copy=True)
    sf.attrs["test"] = "test"
    sf.close()

    # When reopening the file, the attribute should be not saved.
    sf = robofish.io.File(valid_file_path, "r")
    assert "test" not in sf.attrs
    sf.close()


def test_File_without_path_or_worldsize():
    with pytest.raises(AssertionError):
        sf = robofish.io.File()


def test_loading_saving(tmp_path):
    f = tmp_path / "file.hdf5"
    sf = test_multiple_entities()

    assert not f.exists()
    sf.save_as(f, no_warning=True)
    assert f.exists()

    # After saving, the file should still be accessible and valid
    # This was previously possible without reloading the file.
    # It had to change because of windows complications. See issue #19
    sf = robofish.io.File(f, "r+")
    sf.validate()

    # Open the file again and add another entity

    print("Recovered file after saving:\n", sf)
    entity = sf.create_entity("fish", positions=np.ones((100, 2)))
    sf.entity_poses
    entity.poses

    sf.validate()
    sf.close()


def test_file_plot():
    with robofish.io.File(valid_file_path) as f:
        f.plot()
        f.plot(lw_distances=True)


def test_file_plot():
    with robofish.io.File(nan_file_path) as f:
        f.plot()
        f.plot(lw_distances=True)


if __name__ == "__main__":
    # Find all functions in this module and execute them
    all_functions = inspect.getmembers(sys.modules[__name__], inspect.isfunction)
    for key, value in all_functions:
        if str(inspect.signature(value)) == "()":
            value()
