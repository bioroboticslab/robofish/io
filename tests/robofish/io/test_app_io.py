import robofish.io.app as app
from robofish.io import utils
import pytest
import logging

logging.getLogger().setLevel(logging.INFO)


resources_path = utils.full_path(__file__, "../../resources")
h5py_file = utils.full_path(__file__, "../../resources/valid_1.hdf5")


def test_app_validate():
    """This tests the function of the robofish-io-validate command"""

    class DummyArgs:
        def __init__(self, path, output_format):
            self.path = path
            self.output_format = output_format

    # invalid.hdf5 should pass a warning
    with pytest.warns(UserWarning):
        raw_output = app.validate(DummyArgs([resources_path], "raw"))

    # The three files valid.hdf5, almost_valid.hdf5, and invalid.hdf5 should be found.
    assert len(raw_output) == 4

    # invalid.hdf5 should pass a warning
    with pytest.warns(UserWarning):
        app.validate(DummyArgs([resources_path], "human"))


def test_app_print():
    """This tests the function of the robofish-io-validate command"""

    class DummyArgs:
        def __init__(self, path, output_format, full_attrs):
            self.path = path
            self.output_format = output_format
            self.full_attrs = full_attrs

    app.print_file(DummyArgs(h5py_file, "full", True))
    app.print_file(DummyArgs(h5py_file, "shape", True))
    app.print_file(DummyArgs(h5py_file, "full", False))
    app.print_file(DummyArgs(h5py_file, "shape", False))
