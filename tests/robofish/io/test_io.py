import robofish.io


def test_now_iso8061():
    # Example time: 2021-01-05T14:33:40.401000+00:00
    time = robofish.io.now_iso8061()
    assert type(time) == str
    assert len(time) == 32
