import robofish.evaluate.app as app
from robofish.io import utils
import pytest
import logging
from pathlib import Path
import numpy as np

np.seterr(all="raise")

logging.getLogger().setLevel(logging.INFO)

h5py_file_1 = utils.full_path(__file__, "../../resources/valid_1.hdf5")
h5py_file_2 = utils.full_path(__file__, "../../resources/valid_2.hdf5")
nan_file_path = utils.full_path(__file__, "../../resources/nan_test.hdf5")


class DummyArgs:
    def __init__(self, analysis_type, paths, save_path, add_train_data):
        self.paths = paths
        self.names = None
        self.analysis_type = analysis_type
        self.save_path = save_path
        self.labels = None
        self.add_train_data = add_train_data


def test_app_validate(tmp_path):
    """This tests the function of the robofish-io-validate command"""

    for mode in app.function_dict().keys():
        if mode == "all":
            app.evaluate(DummyArgs(mode, [h5py_file_1], tmp_path, False))
            app.evaluate(DummyArgs(mode, [h5py_file_2, h5py_file_2], tmp_path, False))
            app.evaluate(DummyArgs(mode, [nan_file_path], tmp_path, False))
        else:
            app.evaluate(DummyArgs(mode, [h5py_file_1], tmp_path / "image.png", False))
            app.evaluate(
                DummyArgs(
                    mode, [h5py_file_2, h5py_file_2], tmp_path / "image.png", False
                )
            )


def test_app_validate(tmp_path):
    """This tests the function of the robofish-io-validate command"""
    for mode in app.function_dict().keys():
        if mode == "all":
            app.evaluate(DummyArgs(mode, [h5py_file_1], tmp_path, False))
            app.evaluate(DummyArgs(mode, [h5py_file_2, h5py_file_2], tmp_path, False))
        else:
            app.evaluate(DummyArgs(mode, [h5py_file_1], tmp_path / "image.png", False))
            app.evaluate(
                DummyArgs(
                    mode, [h5py_file_2, h5py_file_2], tmp_path / "image.png", False
                )
            )
