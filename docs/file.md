`robofish.io.file.File` objects are the root of the project. The object contains all information about the environment, entities, and time.

In the simplest form we define a new File with a world size in cm and a frequency in hz. Afterwards, we can save it with a path.

```python
import robofish.io

# Create a new robofish io file
f = robofish.io.File(world_size_cm=[100, 100], frequency_hz=25.0)
f.save_as("test.hdf5")
```

The File object can also be generated, with a given path. In this case, we work on the file directly. The `with` block ensures, that the file is validated after the block.

```python
with robofish.io.File(
    "test.hdf5", mode="x", world_size_cm=[100, 100], frequency_hz=25.0
) as f:
    # Use file f here
```

When opening a file with a path, a mode should be specified to describe how the file should be opened.

| Mode 	| Description                            	|
|------	|----------------------------------------	|
| r    	| Readonly, file must exist (default)    	|
| r+   	| Read/write, file must exist            	|
| w    	| Create file, truncate if exists        	|
| x    	| Create file, fail if exists            	|
| a    	| Read/write if exists, create otherwise 	|

## Attributes

Attributes of the file can be added, to describe the contents. 
The attributes can be set like this:
```python
f.attrs["experiment_setup"] = "This file comes from the tutorial."
f.attrs["experiment_issues"] = "All data in this file is made up."
```

Any attribute is allowed, but some cannonical attributes are prepared:<br>
`publication_url, video_url, tracking_software_name, tracking_software_version, tracking_software_url, experiment_setup, experiment_issues`

## Properties
As described in `robofish.io`, all properties of `robofish.io.entity`s can be accessed by adding the prefix `entity_` to the function.

## Plotting
`robofish.io.file.File`s have a built in plotting tool with `robofish.io.file.File.plot()`.
With the option `lw_distances = True` the distance between two fish is represented throught the line width.

```python
import robofish.io 
import matplotlib.pyplot as plt 

fig, ax = plt.subplots(1,2, figsize=(10,5))
f = robofish.io.File("...")
f.plot(ax=ax[0])
f.plot(ax=ax[1], lw_distances=True)
plt.show()
```
![](img/file_plot.png)

For all other options while plotting please check `robofish.io.file.File.plot()`.

