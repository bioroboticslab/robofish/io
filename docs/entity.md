Tracks of entities are stored in `robofish.io.entity.Entity` objects. They are created by the `robofish.io.file.File` object. They require a category and can have a name. If no name is given, it will be created, using the category and an id.

```python
f = robofish.io.File(world_size_cm=[100, 100], frequency_hz=25.0)
nemo = f.create_entity(category="fish", name="nemo")
```

The function `create_entity()` returns an entity object. It can be stored but it's not neccessary. The entity is automatically stored in the file.

## Pose options

The poses of an entity can be passed in multiple ways. Poses are divided into `positions` (x, y) and `orientations`.
Orientations are internally represented in unit vectors but can also be passed in rads.
The shape of the passed orientation array defines the meaning.

```python
# Create dummy poses for 100 timesteps
pos = np.zeros((100, 2))
ori_vec = np.zeros((100,2)) * [0, 1]
ori_rad = np.zeros((100,1))

# Creating an entity without orientations. Here we keep the entity object, to use it later.
f.create_entity(category="fish", positions=pos)
# Creating an entity using orientation vectors. Keeping the entity object is not neccessary, it is saved in the file.
f.create_entity(category="fish", positions=pos, orientations=ori_vec)
# Creating an entity using radian orientations.
f.create_entity(category="fish", positions=pos, orientations=ori_rad)
```

The poses can be also passed in an combined array.
```python
# Create an entity using orientation vectors.
f.create_entity(category="fish", poses=np.ones((100,4)) * np.sqrt(2))
# Create an entity using radian orientations
f.create_entity(category="fish", poses=np.zeros((100,3)))
```

## Creating multiple entities at once

Multiple entities can be created at once.
```python
# Here we create 4 fishes from poses with radian orientation
f.create_multiple_entities(category="fish", poses=np.zeros((4,100,3)))
```

## Attributes

Entities can have attributes to describe them. 

The attributes can be set like this:
```python
nemo.attrs["species"] = "Clownfish"
nemo.attrs["fish_standard_length_cm"] = 10
```

Any attribute is allowed, but some cannonical attributes are prepared:<br>
`species`:`str`, `sex`:`str`, `fish_standard_length_cm`:`float`


## Properties

As described in `robofish.io`, Files and Entities have useful properties.

<small>

| Entity function                                  | Description                                                                            |
| ------------------------------------------------ | -------------------------------------------------------------------------------------- |
| `robofish.io.entity.Entity.positions`            | Positions as a `(timesteps, 2 (x, y))` array.                                          |
| `robofish.io.entity.Entity.orientations`         | Orientations as a `(timesteps, 2 (ori_x, ori_y))` array.                               |
| `robofish.io.entity.Entity.orientations_rad`     | Orientations as a `(timesteps, 1 (ori_rad))` array.                                    |
| `robofish.io.entity.Entity.poses`                | Poses as a `(timesteps, 4 (x, y, x_ori, y_ori))` array.                                |
| `robofish.io.entity.Entity.poses_rad`            | Poses as a `(timesteps, 3(x, y, ori_rad))` array.                                      |
| `robofish.io.entity.Entity.actions_speeds_turns` | Speed and turn as a `(timesteps - 1, 2 (speed in cm/frame, turn in rad/frame))` array. |

</small>

---

⚠️ Try this out by extending the example of the main doc, so that a new teleporting fish with random positions [-50, 50] and orientations (0, 2 * pi) is generated. How does that change the output and speed histogram `robofish-io-evaluate speed example.hdf5`? Try writing some attributes.
