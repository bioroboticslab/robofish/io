This package provides you:

- Creation, storing, loading, modifying, inspecting of io-files.
- Preprocessing (orientation calculation, action calculation, raycasting, ...)
- Quick and easy evaluation of behavior
- Data, which is interchangable between labs and tools. No conversions required, since units and coordinate systems are standardized.
- No custom data import, just `import robofish.io`
- This package is tested extensively

<a href="https://git.imp.fu-berlin.de/bioroboticslab/robofish/io" class="myButton">Code</a>

## Installation
Add our [Artifacts repository](https://git.imp.fu-berlin.de/bioroboticslab/robofish/artifacts) to your pip config and install the packagage.

```bash
python3 -m pip config set global.extra-index-url https://git.imp.fu-berlin.de/api/v4/projects/6392/packages/pypi/simple
python3 -m pip install robofish-io robofish-trackviewer
```

## Usage

This documentation is structured with increasing complexity.
First we'll execute the example from the readme. More examples can be found in ```examples/```.

```python
import robofish.io
import numpy as np

# Create a new robofish io file

f = robofish.io.File(world_size_cm=[100, 100], frequency_hz=25.0)
f.attrs['experiment_setup'] = \
    'This is a simple example with made up data.'

# Create a new fish entity with 10 timesteps.

poses = np.zeros((10, 3))
poses[:, 0] = np.arange(-5, 5)
poses[:, 1] = np.arange(-5, 5)
poses[:, 2] = np.arange(0, 2 * np.pi, step=2 * np.pi / 10)

fish = f.create_entity('fish', poses=poses)
fish.attrs['species'] = 'My rotating spaghetti fish'

# Some possibilities to access the data

print ('The file:', f)
print ('Poses Shape (entities, timesteps, (x, y, ori):')
print (f.entity_poses_rad.shape)
print ('The actions (timesteps, (speed, turn)):')
print (fish.actions_speeds_turns)

# Save the file

f.save_as('example.hdf5')
```

⚠️ Please try out the example on your computer and read the output.

We created an `robofish.io.file.File` object, then we added two `robofish.io.entity.Entity` objects.
Afterwards we read some properties of the file and printed them *(more info in [Reading Properties](#reading-properties))*.
Lastly, we saved the file to `example.hdf5`.
Congrats, you created your first io file. We'll continue working with it.

---

We can examine the file now by using commandline tools. These are some examples, more details in [Commandline Tools](## Commandline Tools)

```bash
robofish-io-print example.hdf5
```
Checking out the file content.

```bash
robofish-io-evaluate speed example.hdf5
```
Show a histogram of speeds in the file. For more evaluation options check `robofish-io-evaluate --help`

```bash
robofish-trackviewer example.hdf5
```
View a video of the track in an interactive window.

```bash
robofish-io-render example.hdf5
```
A dynamic animation of the track with moving camera. (Beta)

Further details about the commandline tools can be found in `robofish.io.app`.

## Accessing real data
Until now, we only worked with dummy data. Data from different sources is available in the Trackdb. It is currently stored at the FU Box.

⚠️ If you don't have access to the Trackdb yet, please text Andi by Mail or Mattermost (andi.gerken@gmail.com)


## Reading properties

Files and entities have usefull properties to access their content. In this way, positions, orientations, speeds, and turns can be accessed easily.

All shown property functions can be called from a file or on one entity.
The function names are identical but have a `entity_` prefix.

```python
f = robofish.io.File(world_size_cm=[100, 100], frequency_hz=25.0)
nemo = f.create_entity(category='fish', name='nemo', poses=np.zeros((10,3)))
dori = f.create_entity(category='fish', name='dori', poses=np.zeros((10,3)))


# Get the poses of nemo. Resulting in a (10,3) array
print(nemo.poses_rad)

# Get the poses of all entities. Resulting in a (2,10,3) array.
print(f.entity_poses_rad)
```

In the same scheme the following properties are available:

<small>

| File/ Entity function         | Description                                                                                        |
| ----------------------------- | -------------------------------------------------------------------------------------------------- |
| *entity_*positions            | Positions as a `(*entities*, timesteps, 2 (x, y))` array.                                          |
| *entity_*orientations         | Orientations as a `(*entities*, timesteps, 2 (ori_x, ori_y))` array.                               |
| *entity_*orientations_rad     | Orientations as a `(*entities*, timesteps, 1 (ori_rad))` array.                                    |
| *entity_*poses                | Poses as a `(*entities*, timesteps, 4 (x, y, x_ori, y_ori))` array.                                |
| *entity_*poses_rad            | Poses as a `(*entities*, timesteps, 3(x, y, ori_rad))` array.                                      |
| *entity_*actions_speeds_turns | Speed and turn as a `(*entities*, timesteps - 1, 2 (speed in cm/frame, turn in rad/frame))` array. |

</small>

All these functions are described in more detail in `robofish.io.entity`.

## Where to continue?
We recommend continuing to read advanced options for `robofish.io.file`s and `robofish.io.entity`s.
Create some files, validate them, look at them in the trackviewer, evaluate them.

If you find bugs or get stuck somewhere, please text `Andi` on Mattermost or by mail ([andi.gerken@gmail.com](mailto:andi.gerken@gmail.com))


---

## Extended functions
In the [Track Format Specification](https://git.imp.fu-berlin.de/bioroboticslab/robofish/track_format/uploads/f76d86e7a629ca38f472b8f23234dbb4/RoboFish_Track_Format_-_1.0.pdf) and in this package there are more possibilities, we don't describe explicitly here. If you want to use any of the following features, please ask Andi:

Built in robofish.io and not described:

- Multiple sampling frequencies
- Timestamps per timestep
- Calendar points
- Outlines
- Obstacles

Planned in Track Format but not implemented:

- 3D Tracks


<style>
.myButton {
	background:#3971cc;
	background-color:#3971cc;
	border-radius:9px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:20px;
	padding:8px 16px;
	text-decoration:none;
}
.myButton:hover {
	background:#396bd1;
	background-color:#396bd1;
  color: #e0e0e0;
  text-decoration: none;
}
.myButton:active {
	position:relative;
	top:1px;
}
</style>
