This module defines the command line interface.
All commands have a ``--help`` option to get more info about them in the command line.


## Print
```bash
robofish-io-print example.hdf5
```
Checking out the content of files.

<small>

```
usage: robofish-io-print [-h] [--output_format {shape,full}] path

This function can be used to print hdf5 files from the command line
positional arguments:
  path                  The path to a hdf5 file
optional arguments:
  -h, --help            show this help message and exit
  --output_format {shape,full}
                        Choose how datasets are printed, either the shapes or the full content is printed
```

</small>

## Evaluate
```bash
robofish-io-evaluate *analysis_type* example.hdf5
```
Show some property of a file.


<small>

```
usage: robofish-io-evaluate [-h] [--names NAMES [NAMES ...]] [--save_path SAVE_PATH]
                            {speed,turn,orientation,relative_orientation,distance_to_wall,tank_positions,trajectories,evaluate_positionVec,follow_iid} paths [paths ...]

This function can be called from the commandline to evaluate files.
Different evaluation methods can be called, which generate graphs from the given files.
With the first argument 'analysis_type', the type of analysis is chosen.

positional arguments:
  {speed,turn,orientation,relative_orientation,distance_to_wall,tank_positions,trajectories,evaluate_positionVec,follow_iid}
                        The type of analysis.
                        speed                - Evaluate the speed of the entities as histogram.
                        turn                 - Evaluate the turn angles of the entities as histogram.
                        orientation          - Evaluate the orientations of the entities on a 2d grid.
                        relative_orientation - Evaluate the relative orientations of the entities as a histogram.
                        distance_to_wall     - Evaluate the distances of the entities to the walls as a histogram.
                        tank_positions       - Evaluate the positions of the entities as a heatmap.
                        trajectories         - Evaluate the trajectories of the entities.
                        evaluate_positionVec - Evaluate the vectors pointing from the focal fish to the conspecifics as heatmap.
                        follow_iid           - Evaluate the follow metric in respect to the inter individual distance (iid).
  paths                 The paths to files or folders. Multiple paths can be given to compare experiments.

optional arguments:
  -h, --help            show this help message and exit
  --names NAMES [NAMES ...]
                        Names, that should be used in the graphs instead of the pahts.
  --save_path SAVE_PATH
                        Filename for saving resulting graphics.
```

</small>

## Trackviewer

```bash
robofish-trackviewer example.hdf5
```

The trackviewer is from a different repository. It was included in the install instructions.

<small>

```
usage: robofish-trackviewer [-h] [--draw-labels] [--draw-view-vectors] [--far-plane FAR_PLANE] [--view-of-agents field of perception number of bins] [--view-of-walls field of perception number of bins]
                            [--view-of-walls-matches]
                            [trackset_file]

View RoboFish tracks in a GUI.

positional arguments:
  trackset_file         Path to HDF5 file containing the tracks to view

optional arguments:
  -h, --help            show this help message and exit
  --draw-labels         Whether to draw labels inside the agents' outlines
  --draw-view-vectors   Whether to draw view vectors to the right of / below the trackfile
  --far-plane FAR_PLANE
                        Maximum distance an agent can see
  --view-of-agents field of perception number of bins
  --view-of-walls field of perception number of bins
  --view-of-walls-matches
```

</small>